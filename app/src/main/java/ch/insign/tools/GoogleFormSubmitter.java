/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools;

import android.os.AsyncTask;
import android.text.TextUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Submit a form entry to a (public) Google form.
 * The data can then be used in the connected Google spreadsheet.
 * 
 * @author bachi
 *
 */
public class GoogleFormSubmitter {

	String formKey;
	
	private static String TAG = "GoogleFormSubmitter";
	
	/**
	 * Field name/value pairs for Google forms. You need to extact the field id from the form's html source.
	 * @author bachi
	 *
	 */
	public class Field {
		String fieldName;
		String fieldValue;
				
		private Field(String fieldName, String fieldValue) {
			this.fieldName = fieldName;
			this.fieldValue = fieldValue;
		}
	}
	
	/**
	 * Instantiate using the Google form key, 
	 * e.g. https://docs.google.com/forms/d/1nSux8vDKLrlAQ1bhjN_TNuZWmIEnpZA0yJaYGySiKHB/viewform, the the form key is:
	 * 1nSux8vDKLrlAQ1bhjN_TNuZWmIEnpZA0yJaYGySiKHB
	 * @param formKey
	 */
	public GoogleFormSubmitter(String formKey) {
		this.formKey = formKey;
		if (TextUtils.isEmpty(formKey)) {
			Logger.e(TAG, "No Google Form Key passed!");
		}
	}
	
	/**
	 * Create a new field, containing the form field's name (see the form's http source) and its value.
	 * @param fieldName
	 * @param fieldValue
	 * @return
	 */
	public Field keyValue(String fieldName, String fieldValue) {
		return new Field(fieldName, fieldValue);
	}
	
	/**
	 * Submit a form value entry to a (public) Google Form
	 */
	public void submit(final Field... fields) {	    
				
		new AsyncTask<String, Void, Void>() {			
			@Override
			protected Void doInBackground(String... params) {
				HttpClient client = new DefaultHttpClient();
				String url = "https://docs.google.com/forms/d/" + params[0] + "/formResponse";
				HttpPost post = new HttpPost(url);

				List<BasicNameValuePair> results = new ArrayList<BasicNameValuePair>();	    
				
				for (Field field : fields) {
					results.add(new BasicNameValuePair(field.fieldName, field.fieldValue));
				}									   
				
				try {
					post.setEntity(new UrlEncodedFormEntity(results));
				} catch (UnsupportedEncodingException e) {					
					Logger.e(TAG, "An error has occurred", e);
				}
				try {
					HttpResponse r = client.execute(post);
					Logger.i(TAG, "Sending entry to Google Form resulted in: " + r.getStatusLine().toString());
				} catch (ClientProtocolException e) {					
					Logger.e(TAG, "client protocol exception", e);
				} catch (IOException e) {					
					Logger.e(TAG, "io exception", e);
				}
				return null;
			};
		}.execute(formKey);			    	   
	}
}
