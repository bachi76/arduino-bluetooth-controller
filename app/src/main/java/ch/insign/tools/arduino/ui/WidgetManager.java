/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

import android.util.SparseArray;
import android.widget.Toast;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import ch.insign.tools.Logger;

/**
 * The widget manager enumerates the available widgets and their exposed commands (using their @Command annotation)
 * and dispatches command strings (e.g. "a1>7>Hello..." to the target objects/methods (using reflection).
 *  
 * @author bachi
 *
 */
public class WidgetManager {
	
	/********* All widget classes need to be listed here! *******/

	@SuppressWarnings("rawtypes")
	protected static final Class[] widgetClasses = {
			WidgetContainer.class,
			WidgetText.class,
			WidgetButton.class,
            WidgetSlider.class,
			WidgetPlot.class,
			WidgetRead.class,
			WidgetWrite.class,
			WidgetTest.class
	};
	
	/************************************************************/

	private UI ui;							

	/** Used if this widget requires instances **/	 
	protected SparseArray<WidgetBase> widgetHolder = new SparseArray<WidgetBase>();
	
	/** Holds a list of all commands and 'pointers' to the method to be executed **/
	protected static HashMap<String, Method> commandList = new HashMap<String, Method>();
	
	private static final String TAG = "WidgetManager";

	/**
	 * Constructor
	 */
	public WidgetManager(UI ui) {
		this.ui = ui;		
		if (commandList.size()==0) buildCommands();
	}

	/**
	 * Show a toast message (sent to the ui thread)
	 * @param text
	 * @param duration
	 */
	public void toast(final String text, final int duration) {
		ui.activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(WidgetManager.this.ui.activity, text, duration).show();							
			}
		});
	}
	
	/**
	 * Get all available widget classes.
	 * @return
	 */
	public static Class[] getWidgetClasses() {
		return widgetClasses;
	}
	
	/**
	 * Loop through all widget classes and build a list of commands
	 * for faster lookup
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void buildCommands() {
		
		String commandListOutput = "";
		
		// Find targeted class and get a working instance
		for (Class clazz : widgetClasses) {
			Command classAnnotation = (Command) clazz.getAnnotation(Command.class);
			
			if (classAnnotation == null) {
				Logger.w(TAG, "Missing annotation for class " + clazz.getName());
				continue;
			}			
			
			for (Method method : clazz.getMethods()) {
				Command methodAnnotation = (Command) method.getAnnotation(Command.class);
				
				if (methodAnnotation == null) continue;
				
				// The command consists of 1 char as class marker and 1 char as method marker
				String command = classAnnotation.value() + methodAnnotation.value();
				
				if (!commandList.containsKey(command)) {
					commandList.put(command, method);
					Logger.i(TAG, String.format("Adding command '%s' for class '%s' method '%s'", command, method.getClass().getName(), method.getName()));
					commandListOutput += (command + ": " + method.toGenericString().replace("public void ch.insign.tools.arduino.ui.", "") + "; ");
					
				} else {
					Logger.w(TAG, "Duplicate entry for command: " + command);
				}				
			}		
		}
		Logger.d(TAG, String.format("Found %n commands.", commandList.size()));
		Logger.i(TAG, "Command list: " + commandListOutput);
	}

	/**
	 * Static entry point. To be overriden in subclasses.
	 * token[0] = the command
	 * token[n>0] = parameters
	 * @param tokens
	 */
	//@SuppressWarnings({ "unchecked", "rawtypes" })
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void handleCommand(String[] tokens) {		

		// Find targeted class and get a working instance
		for (String command : commandList.keySet()) {
			if (tokens[0].equals(command)) {
				Method targetMethod = commandList.get(command);
				Class targetClass = targetMethod.getDeclaringClass();
				WidgetBase widget = null;
				
				// Get us a widget instance
				try {
					widget = (WidgetBase) targetClass.getMethod("getInstance", String[].class, WidgetManager.class).invoke(null, tokens, this);					

				} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
					Logger.e(TAG, e.getCause() != null ? e.getCause().getMessage() : e.getMessage(), e);
				}

				// Invoke the command's target method (if we have a widget)
				if (widget != null) {
					int i=0;					
										
					try {
						//targetMethod.invoke(widget, new Object[]{tokens}); // (http://www.coderanch.com/t/328722/java/java/Passing-array-vararg-method-Reflection)						
						
						// Cast to target type and build params array
						Class[] paramTypes = targetMethod.getParameterTypes();						
						Object[] params = new Object[paramTypes.length];		
						if (tokens.length - 1 != paramTypes.length) {
							Logger.w(TAG, String.format("Call to %s with command '%s' failed, parameter count not matching", targetMethod.toString(), command));
							toast(String.format("Call to %s with command '%s' failed, parameter count not matching", targetMethod.toGenericString(), command), Toast.LENGTH_LONG);
							continue;
						}
						for (Class paramType : paramTypes) {														
							params[i] = castParam(tokens[i+1], paramType);																																		
							i++;
						}
						
						// Call target method
						targetMethod.invoke(widget, params);
						Logger.d(TAG, String.format("Called target method '%s' for command '%s'", targetMethod.toString(), command));
						
					} 
					catch (ClassCastException e) {						 
						Logger.w(TAG, String.format("Call to %s with command '%s' failed due to a parameter casting error: %s", targetMethod.toString(), command, e.getMessage()));
						toast(String.format("Call to %s with command '%s' failed due to a parameter casting error: %s", targetMethod.toString(), command, e.getMessage()), Toast.LENGTH_LONG);
						e.printStackTrace();
					}
					catch (IllegalArgumentException e) {						 
						e.printStackTrace(); 
					} catch (IllegalAccessException e) { 
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}							
			}			
		}		
	}

	/**
	 * Cast string input param to the desired output type.
	 * If a parameter type is not supported, a ClassCast exception will be thrown.
	 * 
	 * @param input
	 * @param paramType
	 * @return
	 * @throws ClassCastException
	 */
	@SuppressWarnings("unchecked")
	private <T> T castParam(String input, @SuppressWarnings("rawtypes") Class paramType) throws ClassCastException {
		
		boolean casted = false;
		T output = null;
		
		if (paramType == String.class) {
			output = (T) input;
			casted = true;
		}
		
		if (paramType == Integer.class || paramType == int.class) {
			output = (T) Integer.valueOf(input);
			casted = true;
		}
		
		if (paramType == Double.class || paramType == double.class) {
			output = (T) Integer.valueOf(input);
			casted = true;
		}
		
		if (paramType == Float.class || paramType == float.class) {
			output = (T) Float.valueOf(input);
			casted = true;
		}
		
		if (paramType == Boolean.class || paramType.getName().equals("boolean")) {
			if (input.equals("1")) input = "true";
			output = (T) ((Boolean) Boolean.parseBoolean(input));
			casted = true;
		}
		
		if (!casted) {
			throw new ClassCastException("Unsupported parameter type: " + paramType.getName());
		}
		
		return output;
	}

	/**
	 * Get a previously instantiated widget (not view)
	 * Note: Arduino communicates with widgets, not views. A widget can control 0-n views 
	 * 
	 * @param id
	 * @return
	 */
	public WidgetBase getWidget(int id) {
		return widgetHolder.get(id);
	}

	/**
	 * Put a widget into the widget holder
	 * @param id
	 * @param widget
	 */
	public void putWidget(int id, WidgetBase widget) {
		widgetHolder.put(id, widget);
	}

	/**
	 * Return the UI class instance
	 * @return
	 */
	public UI getUi() {
		return ui;
	}

}
