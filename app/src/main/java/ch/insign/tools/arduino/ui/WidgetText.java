/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import ch.insign.tools.Logger;
import ch.insign.tools.arduino.ArduinoBT;

/**
 * Implements a text field
 * @author bachi
 *
 */
@Command("a")
public class WidgetText extends WidgetBase {

	private static final String TAG = "WidgetText";
	protected TextView tv;

	/**
	 * Constructor
	 * @param manager
	 */
	public WidgetText(WidgetManager manager) {
		super(manager);		
	}

	/**
	 * Static widget instance dispatcher
	 * @param tokens
	 */	
	public static WidgetText getInstance(String[] tokens, WidgetManager manager) {

		int id = Integer.parseInt(tokens[1]);						

		WidgetText instance = (WidgetText) manager.getWidget(id);
		if (instance == null) {
			instance = new WidgetText(manager);
			manager.putWidget(id, instance);
		}

		return instance;				
	}

	@Command("1")
	@Function("addText")
	public void create(int id, String label, int maxLines, int textSize, int gravity) {

		try {

			if (maxLines == 0) maxLines = 1;			
			boolean createNew = false;
			if (tv == null) createNew = true;

			if (createNew) tv = new TextView(manager.getUi().getActivity());

			tv.setMaxLines(maxLines);
			tv.setText(label);		
			tv.setTextSize(textSize);
			tv.setGravity(gravity);

			tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

			if (createNew) manager.getUi().appendViewToRow(id, tv);


		} catch (Exception e) {
			Logger.w(TAG, "Cannot add textview for some reason: " + e.getMessage());
			e.printStackTrace();
		}						
	}

	/**
	 * Update the text of an existing text widget
	 * @param id
	 * @param label
	 */
	@Command("2")
	public void update(int id, final String label) {
		if (tv != null) {
			tv.post(new Runnable() {				
				@Override
				public void run() {
					tv.setText(label);					
				}
			});
			
		} else {
			Logger.w(TAG, "Cannot update non-existing text view");
		}
	}

	/**
	 * Bind an existing text widget to an arduino channel. Any updates on the channel will be displayed.
	 * @param id
	 * @param channel
	 */
	@Command("3")	
	public void bind(int id, int channel) {
		if (tv == null) {
			Logger.w(TAG, "Cannot bind non-existing text view");
		}

		// Create a arduino bt receiver for the selected channel
		ArduinoBT.ReceiveListener receiver = new ArduinoBT.ReceiveListener(channel) {			

			@Override
			public void receive(int key, final String command) {				
				tv.post(new Runnable() {				
					@Override
					public void run() {
						tv.setText(command);					
					}
				});				
			}
		};
		
		// Add the data receive listener to our Arduino class.
		manager.getUi().getArduino().addReceiveListener(receiver);
	}





}
