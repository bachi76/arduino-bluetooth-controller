/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

import android.widget.SeekBar;
import android.widget.TableRow.LayoutParams;

import ch.insign.tools.Logger;

/**
 * Implements a button
 * @author bachi
 *
 */
@Command("s")
public class WidgetSlider extends WidgetBase {

	private static final String TAG = "WidgetSlider";

	static final String REMOTE_CMD_SLIDER_CHANGED = "s1";

	protected SeekBar seekBar;
    protected int min;
    protected int max;
    protected int stepSize;

	/**
	 * Static widget instance dispatcher
	 * @param manager
	 * @return
	 */
	public WidgetSlider(WidgetManager manager) {
		super(manager);		
	}
		
	/**
	 * Static widget instance dispatcher
	 * @param tokens
	 */	
	public static WidgetSlider getInstance(String[] tokens, WidgetManager manager) {
	
		int id = Integer.parseInt(tokens[1]);						
		
		WidgetSlider instance = (WidgetSlider) manager.getWidget(id);
		if (instance == null) {
			instance = new WidgetSlider(manager);
			manager.putWidget(id, instance);
		}
		
		return instance;				
	}

	@Command("1")
	@Function("addSlider")
	public void create(int id, final int min, int max, int initial, int stepSize) {

        this.max = max;
        this.min = min;
        this.stepSize = stepSize;

		try {								
			boolean createNew = false;
			if (seekBar == null) createNew = true;
			if (createNew) {

                seekBar = new SeekBar(manager.getUi().getActivity());
                seekBar.setTag(id);

                seekBar.setMax(max - min);
                seekBar.setProgress(initial - min);

//                ShapeDrawable thumb = new ShapeDrawable(new OvalShape());
//                thumb.setIntrinsicHeight(80);
//                thumb.setIntrinsicWidth(30);
//                seekBar.setThumb(thumb);
//                seekBar.setVisibility(View.VISIBLE);
//                seekBar.setBackgroundColor(Color.BLUE);

                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        // TODO: snap to stepSize
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    public void onStopTrackingTouch(SeekBar arg0) {

                        int value = min + seekBar.getProgress();

                        String cmd =
                                REMOTE_CMD_SLIDER_CHANGED + UI.PARAM_DELIM +
                                seekBar.getTag() + UI.PARAM_DELIM +
                                value;

						manager.getUi().sendResponse(cmd);
                    }

                });
            }

			seekBar.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

			if (createNew) manager.getUi().appendViewToRow(id, seekBar);
			
			
		} catch (Exception e) {
			Logger.w(TAG, "Cannot add seekBar for some reason: " + e.getMessage());
			e.printStackTrace();
		}
	}

    @Command("2")
    @Function("setSlider")
    public void set(int id, int value) {
        seekBar.setProgress(value);
    }

}
