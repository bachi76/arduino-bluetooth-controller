/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

/**
 * Implements the container commands. 
 * Type: Singleton
 * @author bachi
 *
 */
@Command("c")
public class WidgetContainer extends WidgetBase {
	
	private static final String TAG = "WidgetContainer";
	
	static protected WidgetContainer instance;

	private static int STATIC_WIDGET_ID = -1; 
	
	
	
	/**
	 * Constructor
	 * @param manager
	 */
	public WidgetContainer(WidgetManager manager) {
		super(manager);		
	}
		
	/**
	 * Static widget instance dispatcher
	 * @param tokens
	 * @param manager
	 * @return
	 */
	public static WidgetContainer getInstance(String[] tokens, WidgetManager manager) {
			
		WidgetContainer instance = (WidgetContainer) manager.getWidget(STATIC_WIDGET_ID);
		if (instance == null) {
			instance = new WidgetContainer(manager);
			manager.putWidget(STATIC_WIDGET_ID, instance);
		}
		
		return instance;				
	}

	// TODO: Completely move the logic from UI to this widget
	@Command("1")
	public void addNewTable(boolean stretchable) {		
		manager.getUi().addNewTable(stretchable);
	}
	
	// TODO: Completely move the logic from UI to this widget
	@Command("2")
	public void addNewRow() {	
		manager.getUi().addNewRow();
	}
	
	// TODO: Completely move the logic from UI to this widget
	@Command("3")
	public void resetUI() {
		manager.getUi().resetUI();
	}
	
	
	
	

}
