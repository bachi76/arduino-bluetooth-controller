/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import ch.insign.tools.EasyHttpClient;
import ch.insign.tools.Logger;

/**
 * Provides read access to various information to the Arduino, such as the current time
 * Type: Singleton
 * @author bachi
 *
 */
@Command("r")
public class WidgetRead extends WidgetBase {

    private static final String TAG = "WidgetRead";

    static protected WidgetRead instance;

    private static int STATIC_WIDGET_ID = -3;

    static final String REMOTE_CMD_READ = "r";



    /**
     * Constructor
     * @param manager
     */
    public WidgetRead(WidgetManager manager) {
        super(manager);
    }

    /**
     * Static widget instance dispatcher
     * @param tokens
     */
    public static WidgetRead getInstance(String[] tokens, WidgetManager manager) {

        WidgetRead instance = (WidgetRead) manager.getWidget(STATIC_WIDGET_ID);
        if (instance == null) {
            instance = new WidgetRead(manager);
            manager.putWidget(STATIC_WIDGET_ID, instance);
        }

        return instance;
    }

    /**
     * Get the current time from the phone.
     * Format: HH:mm:ss
     *
     * @param responseId
     */
    @Command("1")
    public void time(int responseId) {
        SimpleDateFormat fmt = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        String dateString = fmt.format(date);

        Logger.d(TAG, "Time requested. Returning: " + dateString);
        String cmd =
                REMOTE_CMD_READ + UI.PARAM_DELIM +
                        String.valueOf(responseId) + UI.PARAM_DELIM +
                        dateString;

        manager.getUi().sendResponse(cmd);
    }

    /**
     * Fetch a url and return its content.
     *
     * Hint:
     * Use a Google Spreadsheet and its 'publish' feature for easy web-config values.
     * Create an url for each config value (use the range parameter in GDrive)
     *
     * If updatePeriod is > 0, then the widget will re-check the url every n seconds
     * and if the content has changed, deliver it again.
     *
     * @param responseId
     * @param url
     * @param updatePeriod in sec
     */
    @Command("2")
    public void url(final int responseId, final String url, int updatePeriod) {

        ReadTask task = new ReadTask(responseId, manager) {

            @Override
            protected String onUpdate() {
                String content = null;

                EasyHttpClient client = new EasyHttpClient();
                content = client.get(url);
                if (content == null) {
                    manager.toast("Error fetching url.", Toast.LENGTH_SHORT);
                    Logger.e(TAG, "Error fetching url: " + url);
                    return null;
                }

                Logger.d(TAG, "URL content requested. Got: " + content);

                String cmd =
                        REMOTE_CMD_READ + UI.PARAM_DELIM +
                                String.valueOf(responseId) + UI.PARAM_DELIM +
                                content;

                return cmd;
            }
        };

        task.setUpdatePeriod(updatePeriod * 1000);
        task.run();

    }


    /**
     * Fetch a url and return its content.
     *
     * Hint:
     * Use a Google Spreadsheet and its 'publish' feature for easy web-config values.
     * Create an url for each config value (use the range parameter in GDrive)
     *
     * If updatePeriod is > 0, then the widget will re-check the url every n seconds
     * and if the content has changed, deliver it again.
     *
     * @param responseId
     * @param axis 'X', 'Y' or 'Z'
     * @param updatePeriod in ms
     */
    @Command("3")
    public void acceleration(final int responseId, final String axis, final int updatePeriod, final int multiplier) {

        ReadTask task = new ReadTask(responseId, manager) {

            float x = 0;
            float y = 0;
            float z = 0;

            @Override
            protected void onCreate() {

                // FIXME: Should ensure we only register max 1 listener
                // FIXME: Also ensure proper listener removal

                SensorEventListener accelerometerListener = new SensorEventListener() {

                    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

                    public void onSensorChanged(SensorEvent event) {
                        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                            x = event.values[0];
                            y = event.values[1];
                            z = event.values[2];

                            //Logger.d(TAG, String.format("Acc: x: %f / y: %f / z: %f", x,y,z));
                        }
                    }
                };

                SensorManager sensorManager = (SensorManager) manager.getUi().activity.getSystemService(Context.SENSOR_SERVICE);
                sensorManager.registerListener(accelerometerListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);

            }

            @Override
            protected String onUpdate() {
                String content;
                float value;

                if (axis.equalsIgnoreCase("X")) {
                    value = x;
                } else if (axis.equalsIgnoreCase("Y")) {
                    value = y;
                } else if (axis.equalsIgnoreCase("Z")) {
                    value = z;
                } else {
                    Logger.e(TAG, "Invalid axis: " + axis);
                    return null;
                }

                content = String.valueOf(Math.round(value * multiplier));

                Logger.d(TAG, axis + " axis requested. Got: " + content);

                String cmd =
                        REMOTE_CMD_READ + UI.PARAM_DELIM +
                                String.valueOf(responseId) + UI.PARAM_DELIM +
                                content;

                return cmd;
            }
        };

        task.setUpdatePeriod(updatePeriod);
        task.run();

    }


}
