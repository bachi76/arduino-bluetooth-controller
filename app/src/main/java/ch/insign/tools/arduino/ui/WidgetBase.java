/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

import ch.insign.tools.Logger;

/**
 * Base class for all widgets.
 * Override at least getInstance() for every widget.
 * 
 * Each subclass must annotate (@Command) the class with a single and unique letter, and each exposed method with another 
 * single unique letter (typically numbers starting from 1). Both letters form the command id which addresses the target method.  
 * 
 * 
 * @author bachi
 */
public class WidgetBase {
	
	protected WidgetManager manager;
	private static String TAG ="WidgetBase";
	
	/**
	 * Constructor
	 * @param manager
	 */
	public WidgetBase(WidgetManager manager) {
		this.manager = manager;
	}
	
	/**
	 * Static widget instance dispatcher
	 * 
	 * Overwrite this method in each subclass!
	 * Decide whether a singleton instance is enough or different instances are required.
	 * 
	 * @param tokens
	 * @param manager
	 * @return
	 */
	public static WidgetBase getInstance(String tokens[], WidgetManager manager) {		
		Logger.w(TAG, "getInstance not implemented in subclass!");
		return null;		
	}
}
