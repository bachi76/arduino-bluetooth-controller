/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Handler;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;

import ch.insign.tools.Logger;
import ch.insign.tools.arduino.ArduinoBT;
import ch.insign.tools.arduino.ArduinoBT.ReceiveListener;

/**
 * Controlls the UI and command flow between the widgets and the remote side.
 * Can connect to an ArduinoBT instance.
 * 
 * Multiple UI instances are possible (e.g. for multiple activities)
 * 
 * @author bachi
 *
 */
public class UI {

	protected Activity activity;
	protected ViewGroup parentLayout;
	protected ArduinoBT arduino;
	protected int channel;

	protected TableLayout currentTable;
	protected TableRow currentTableRow;
		
	private WidgetManager widgetManager;

	protected static final String TAG = "UI";
	protected static final String PARAM_DELIM = ">";
	
	// Commands to remote
	protected static final String REMOTE_CMD_NEED_UI = "c1";
	
	private boolean isConnected = false;
	private boolean pendingSetTableStretchable = false;	

	public UI(Activity activity, int parentLayoutId) {
		
		this.widgetManager = new WidgetManager(this);
		
		this.activity = activity;	
		ViewGroup parentLayout = (ViewGroup) activity.findViewById(parentLayoutId);
		if (parentLayout == null) {
			throw new IllegalArgumentException("Could not find the parentLayout!");
		}		
		
		this.parentLayout = parentLayout;

		// Create a first table (and row)
		addNewTable(true);
		//addNewRow();
	}

	/**
	 * Connect the UI instance to an ArduinoBT instance.
	 * The Arduino needs to be already connected. 
	 * Choose a listen channel above channel 0
	 * 
	 * @param arduino
	 * @return
	 */
	public boolean connectToArduino(ArduinoBT arduino, int channel) {
		
		/* Disabled for testing with inject..
		if (!arduino.isConnected()) {
			Logger.w(TAG, "ArduinoBT not connected, cannot connect UI");
			return false;
		} */
		
		if (isConnected) {
			Logger.i(TAG, "ui instance already connected to arduino");
			return true;
		}
		

		final Handler handler = new Handler();
		
		// Add the listener to the arduino
		ReceiveListener listener = new ReceiveListener(channel) {

			@Override
			public void receive(int key, final String command) {
				
				handler.post(new Runnable() {
					public void run() {
						executeCommand(command);
					}
				});
			}
		};

		arduino.addReceiveListener(listener);
		 
		this.arduino = arduino;
		this.channel = channel;		
		isConnected = true;
		
		// Cancel any pending timer tasks
		ReadTask.cancelAll();
				
		// We're ready to receive the UI setup - tell the Arduino
		arduino.send(channel, REMOTE_CMD_NEED_UI);
				
		return true;
	}

	/**
	 * Execute a command string.
	 * Format: "command_id\param_1\param_n"
	 * 
	 * Returns true if command was executed successfully, false otherwise
	 * See log for possible problems.
	 * To add your own commands to a subclass, overwrite onExecuteCommand()
	 * 
	 * @param command
	 * @return success?
	 */
	public void executeCommand(String command) {
		
		Logger.i(TAG, "Received:" + command);
		
		// Command structure:
		// "command_id>param_1>param_n"
		if (command == null || command.equals("")) {
			Logger.w(TAG, "Empty command string, ignoring.");
			return;
		}
		String[] tokens = TextUtils.split(command, PARAM_DELIM);
		if (tokens.length == 0) {
			Logger.w(TAG, "Could not parse command: " + command);
			return;
		}

		widgetManager.handleCommand(tokens);								
	}
	
	/**
	 * Send a message to the remote side. 
	 * 
	 * Note: Other receivers than only arduino might be possible later,
	 * so ensure widges use this method and not directly the arduino instance.
	 * 
	 * @param msg
	 * @return
	 */
	public boolean sendResponse(String msg) {
		if (arduino == null || !arduino.isConnected()) {
            Logger.w(TAG, "Response '" + msg + "' not sent (not connected)");
            return false;
        }
		return arduino.send(channel, msg);
	}

	public void addNewRow() {

		try {				
			currentTableRow = new TableRow(activity);
			currentTableRow.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			currentTable.addView(currentTableRow);

		} catch (Exception e) {
			Logger.w(TAG, "Cannot add new row: " + e.getMessage());
			e.printStackTrace();
		}				
	}


	public void addNewTable(boolean stretchable) {

		try {						
			
			currentTable = new TableLayout(activity);
			// cannot do currentTable.setStretchAllColumns(stretchable); here, as it throws a division by zero exception			
			pendingSetTableStretchable = stretchable;
			currentTable.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			parentLayout.addView(currentTable);
			addNewRow();

		} catch (Exception e) {
			Logger.w(TAG, "Cannot add new table: " + e.getMessage());
			e.printStackTrace();
		}				
	}
	
	public void resetUI() {
		parentLayout.removeAllViews();
		widgetManager.widgetHolder.clear();
		addNewTable(true);
	}



	/**
	 * Add a built view to the current table row and set a reference in the viewHolder
	 * You can fetch the view again using getView(id) 
	 * @param id
	 * @param v
	 * @return
	 */
	protected boolean appendViewToRow(int id, View v) {

		if (currentTableRow == null) {
			Logger.e(TAG, "Cannot add new view, have no current table row.");
			return false;
		}
//		if (viewHolder.get(id) != null) {
//			Logger.w(TAG, "View with id " + id + " was already added, ignoring this one.");
//			return false;
//		}
				
		currentTableRow.addView(v);		
		
		// A stretchable table seems only to stretch once views have been added(?)
		if (pendingSetTableStretchable) {
			currentTable.setStretchAllColumns(true);
			pendingSetTableStretchable = false;
		}
		
		return true;
	}


	/**
	 * Convert dp to pixel
	 * Use this for all view properties that require pixel instead of dp.
	 * 
	 * @param dp
	 * @return pixel
	 */
	private int dpToPx(int dp) {
		Resources r = activity.getResources();
		return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14, r.getDisplayMetrics()));
	}

	public Activity getActivity() {
		return activity;
	}
	
	/**
	 * Get the arduino bluetooth instance.
	 * 
	 * @return
	 */
	public ArduinoBT getArduino() {
		return arduino;
	}


	// *** just preserving an example of how to add a linear layout with 2 child views
	//	public void addTextView(int id, String label, String initialValue, int maxLines) {
	//		
	//		if (maxLines == 0) maxLines = 1;
	//		
	//		LinearLayout ll = new LinearLayout(activity);
	//		ll.setOrientation(LinearLayout.HORIZONTAL);						
	//		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
	//		
	//		TextView tvLabel = new TextView(activity);
	//		tvLabel.setMaxLines(maxLines);
	//		tvLabel.setText(label);
	//		tvLabel.setPadding(0, 0, dpToPx(10), 0);
	//		tvLabel.setTextSize(18);
	//		
	//		
	//		TextView tvValue = new TextView(activity);
	//		tvValue.setMaxLines(maxLines);
	//		tvValue.setText (initialValue);
	//		tvValue.setTextSize(18);
	//		
	//		ll.addView(tvLabel, layoutParams);
	//		ll.addView(tvValue, layoutParams);
	//		currentTableRow.addView(ll);
	//	}

}
