/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

import ch.insign.tools.Logger;

/**
 * A test playground. Based on a singleton widget.
 * Type: Singleton
 * @author bachi
 *
 */
@Command("t")
public class WidgetTest extends WidgetBase {
	
	private static final String TAG = "WidgetTest";
	
	static protected WidgetTest instance;

	private static int STATIC_WIDGET_ID = -2; 
	
	
	
	/**
	 * Constructor
	 * @param manager
	 */
	public WidgetTest(WidgetManager manager) {
		super(manager);		
	}
		
	/**
	 * Static widget instance dispatcher
	 * @param tokens
	 */	
	public static WidgetTest getInstance(String[] tokens, WidgetManager manager) {
			
		WidgetTest instance = (WidgetTest) manager.getWidget(STATIC_WIDGET_ID);
		if (instance == null) {
			instance = new WidgetTest(manager);
			manager.putWidget(STATIC_WIDGET_ID, instance);
		}
		
		return instance;				
	}

	@Command("1")
	public void testParameterCasting(int t_int, Integer t_integer, float t_float, Float t_Float, double t_double, Double t_Double, String t_String, boolean t_boolean, Boolean t_Boolean) {		
		Logger.d(TAG, "testParameterCasting successful.");
	}
	
	
}
