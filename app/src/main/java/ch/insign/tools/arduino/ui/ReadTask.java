/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

import android.util.SparseArray;

import java.util.Timer;
import java.util.TimerTask;

import ch.insign.tools.Logger;

/**
 * Create ReadTask subclasses for any read values. Allows for single or periodical executions.
 * If the result of onUpdate() changes, it will be sent to the client.
 * 
 * @author bachi
 *
 */
abstract public class ReadTask {
	
	private int responseId;
	private int updatePeriod; // ms
	private Timer timer;
	private WidgetManager manager;
	
	static final private SparseArray<ReadTask> instanceList = new SparseArray<ReadTask>();
	private static final String TAG = "ReadTask";

	public ReadTask(int responseId, WidgetManager manager) {
		this.manager = manager; // FIXME: These back references should be checked for possible gc/mem leak issues
		
		ReadTask oldTask = instanceList.get(responseId);
		
		if (oldTask != null) {
			Logger.d(TAG, "Replacing existing ReadTask with responseId " + responseId);
			oldTask.onCancel();
		} else {
			Logger.d(TAG, "Add new ReadTask with responseId " + responseId);
		}			
		
		instanceList.put(responseId, this);

        onCreate();
	}
	
	/**
	 * Start the execution of this ReadTask
	 */
	public void run() {
		
		TimerTask task = new TimerTask() {
			String prevCmd;
			
			@Override
			public void run() {
				// Send the response if the content has changed since the last time
				String cmd = ReadTask.this.onUpdate();
				if (cmd != null && !cmd.equals(prevCmd)) {					
					manager.getUi().sendResponse(cmd);
					prevCmd = cmd;
				}
			}
		};
				
		if (timer != null) timer.cancel();		
		
		timer = new Timer();
		if (updatePeriod > 0) {
			timer.scheduleAtFixedRate(task, 0, updatePeriod);
		} else {
			timer.schedule(task, 0);
		}			
	}

    /**
     * After the ReadTask was created, before the timer is started.
     */
    protected void onCreate() {};
	
	/**
	 * Called once or many times depending on the given updatePeriod.
	 * Fetch and return the required content (as complete command).
	 * 
	 * It is ok to return the same content multiple times, ReadTask will only deliver changed content to the client.
	 * 
	 * Remember: This is executed in a timer background thread
	 *  
	 * @return the command/content to be returned to the client. Null to indicate no change or an error.
	 */
	protected abstract String onUpdate();

	
	/**
	 * Cancel this ReadTask
	 */
	public void onCancel() {
		if (timer != null) timer.cancel();
	}
		
	/**
	 * Cancel all ReadTasks
	 */
	public static void cancelAll() {
		for (int i = 0; i < instanceList.size(); i++) {
			instanceList.valueAt(i).onCancel();			
		}		
		instanceList.clear();
	}
	
	/**
	 * @return the updatePeriod
	 */
	public int getUpdatePeriod() {
		return updatePeriod;
	}

	/**
	 * @param updatePeriod the updatePeriod to set
	 */
	public void setUpdatePeriod(int updatePeriod) {
		this.updatePeriod = updatePeriod;
	}

	/**
	 * @return the responseId
	 */
	public int getResponseId() {
		return responseId;
	}

	/**
	 * @return all instances that are currently in use
	 */
	public static SparseArray<ReadTask> getInstancelist() {
		return instanceList;
	}
	
	

}
