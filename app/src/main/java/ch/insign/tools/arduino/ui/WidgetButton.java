/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableRow.LayoutParams;

import ch.insign.tools.Logger;

/**
 * Implements a button
 * @author bachi
 *
 */
@Command("b")
public class WidgetButton extends WidgetBase {
	
	private static final String TAG = "WidgetButton";
	
	static final String REMOTE_CMD_BUTTON_PRESSED = "b1";
	
	protected Button button;
	
	/**
	 * Static widget instance dispatcher
	 * @param manager
	 * @return
	 */
	public WidgetButton(WidgetManager manager) {
		super(manager);		
	}
		
	/**
	 * Static widget instance dispatcher
	 * @param tokens
	 */	
	public static WidgetButton getInstance(String[] tokens, WidgetManager manager) {
	
		int id = Integer.parseInt(tokens[1]);						
		
		WidgetButton instance = (WidgetButton) manager.getWidget(id);
		if (instance == null) {
			instance = new WidgetButton(manager);
			manager.putWidget(id, instance);
		}
		
		return instance;				
	}

	@Command("1")
	@Function("addButton")
	public void create(int id, String label) {
		
		try {								
			boolean createNew = false;
			if (button == null) createNew = true;			
			if (createNew) {
				button = new Button(manager.getUi().getActivity());
						
				button.setTag(id);
				button.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {						
						String cmd = REMOTE_CMD_BUTTON_PRESSED + UI.PARAM_DELIM + v.getTag();
						manager.getUi().sendResponse(cmd);								 
					}
				});
			}
			
			button.setText(label);
			button.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

			if (createNew) manager.getUi().appendViewToRow(id, button);
			
			
		} catch (Exception e) {
			Logger.w(TAG, "Cannot add button for some reason: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
