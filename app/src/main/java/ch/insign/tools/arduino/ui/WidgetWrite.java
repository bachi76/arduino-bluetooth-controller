/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

import org.json.JSONException;

import ch.insign.tools.CosmDataOut;
import ch.insign.tools.GoogleFormSubmitter;
import ch.insign.tools.Logger;
import ch.insign.tools.arduino.ArduinoBT;

/**
 * Write out Arduino data to various consumers.
 * Type: Singleton
 * @author bachi
 *
 */
@Command("w")
public class WidgetWrite extends WidgetBase {

	private static final String TAG = "WidgetWrite";
	private static int STATIC_WIDGET_ID = -4;


	/**
	 * Static widget instance dispatcher
	 * @param manager
	 * @return
	 */
	public WidgetWrite(WidgetManager manager) {
		super(manager);		
	}

	/**
	 * Static widget instance dispatcher
	 * @param tokens
	 */	
	public static WidgetWrite getInstance(String[] tokens, WidgetManager manager) {

		WidgetWrite instance = (WidgetWrite) manager.getWidget(STATIC_WIDGET_ID);
		if (instance == null) {
			instance = new WidgetWrite(manager);
			manager.putWidget(STATIC_WIDGET_ID, instance);
		}

		return instance;				
	}

	@Command("1")
	public void toFile(int channel, String filename, boolean append) {
		// TODO
		Logger.e(TAG, "toFile: UNIMPLEMENTED");
	}

	@Command("2")
	public void toURL(int channel, String urlTemplate) {
		// TODO
		Logger.e(TAG, "toURL: UNIMPLEMENTED");
	}

	/**
	 * Send data to a Cosm (www.cosm.com) feed
	 * Note: Every value is sent directly, there's (currently) no buffering
	 * TODO: Add optional buffer size param to stack http requests
	 * 
	 * @param channel to pick up data from
	 * @param feed Cosm feed to be used
	 * @param cosmStreamId Cosm stream id (data set id, starting from 0) - a feed can have multiple streams
	 * @param apikey the cosm api key
	 */
	@Command("3")
	public void toCosm(int channel, final String feed, final int cosmStreamId, final String apikey) {

		// Create a arduino bt receiver for the selected channel		
		ArduinoBT.ReceiveListener receiver = new ArduinoBT.ReceiveListener(channel) {									

			CosmDataOut cosmClient = new CosmDataOut(apikey, feed);
			
			@Override
			public void receive(int key, String command) {								
				try {
					cosmClient.setStream(cosmStreamId, command);
				} catch (JSONException e) {					
					e.printStackTrace();
				}				
			}			
		};
						
		// Add the data receive listener to our Arduino class.
		manager.getUi().getArduino().addReceiveListener(receiver);
	}
	
	@Command("4")
	public void toGoogleSpreadsheet(int channel, final String formKey, final String fieldKey) {

		// Create a arduino bt receiver for the selected channel		
		ArduinoBT.ReceiveListener receiver = new ArduinoBT.ReceiveListener(channel) {												
			GoogleFormSubmitter gform = new GoogleFormSubmitter(formKey);
			
			@Override
			public void receive(int key, String command) {								
				gform.submit(gform.keyValue(fieldKey, command));				
			}			
		};
						
		// Add the data receive listener to our Arduino class.
		manager.getUi().getArduino().addReceiveListener(receiver);
	}

	@Command("5")
	public void toLog(String message) {
		Logger.i("ARDUINO", message);
	}


	@Command("99")
	public void stop(int id, String label) {

	}
}
