/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools.arduino.ui;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.widget.TableRow.LayoutParams;

import com.androidplot.Plot;
import com.androidplot.Plot.BorderStyle;
import com.androidplot.series.XYSeries;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYStepMode;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ch.insign.arduino.R;
import ch.insign.tools.Logger;
import ch.insign.tools.Tools;
import ch.insign.tools.arduino.ArduinoBT;

/**
 * Implements a button
 * @author bachi
 *
 */
@Command("p")
public class WidgetPlot extends WidgetBase {

	public final static String DELIM_XY = ":";
	
	private static final String TAG = "WidgetPlot";	
	static HashMap<Integer, Datasource> datasourceHolder = new HashMap<Integer, Datasource>();

	//static final int CMD_BUTTON_PRESSED = 2;

	protected XYPlot plot;

	/**
	 * Constructor
	 * @param manager
	 */
	public WidgetPlot(WidgetManager manager) {
		super(manager);		
	}

	/**
	 * Static widget instance dispatcher
	 * @param tokens
	 * @param manager
	 * @return
	 */
	public static WidgetPlot getInstance(String[] tokens, WidgetManager manager) {
				
		int id = Integer.parseInt(tokens[1]);						

		WidgetPlot instance = (WidgetPlot) manager.getWidget(id);
		if (instance == null) {
			instance = new WidgetPlot(manager);
			manager.putWidget(id, instance);
		}

		return instance;				
	}

	/**
	 * Create a new line plot.
	 * @param id
	 * @param label
	 * @param dpHeight in density independent android pixels. 0 = take what's available
	 */
	@Command("1")
	public void createLinePlot(int id, String label, int dpHeight) {

		try {								
			boolean createNew = (plot == null);

			if (createNew) {
				LayoutInflater inflater = LayoutInflater.from(manager.getUi().getActivity());				
				plot = (XYPlot) inflater.inflate(R.layout.view_xyplot, null);													
				plot.setTag(id);				
			}
			
			plot.setTitle(label);

			// If height is 0, use WRAP_CONTENT (which hoever consumes the full vertical space that is left)
			int plotHeight;
			if (dpHeight > 0) {
				plotHeight = Tools.dpToPx(dpHeight);
			} else {
				plotHeight = LayoutParams.WRAP_CONTENT;
			}
			
			plot.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, plotHeight));
			
			if (createNew) manager.getUi().appendViewToRow(id, plot);

			// only display whole numbers in domain labels
			plot.getGraphWidget().setDomainValueFormat(new DecimalFormat("0"));

			//plot.addSeries(sine1Series, new LineAndPointFormatter(Color.rgb(0, 0, 0), null, Color.rgb(0, 80, 0)));

			// create a series using a formatter with some transparency applied:
			//LineAndPointFormatter f1 = new LineAndPointFormatter(Color.rgb(0, 0, 200), null, Color.rgb(0, 0, 80));
			//f1.getFillPaint().setAlpha(220);
			//plot.addSeries(sine2Series, f1);
			plot.setGridPadding(5, 0, 5, 0);

			// hook up the plotUpdater to the data model:
			//data.addObserver(plotUpdater);

			plot.setDomainStepMode(XYStepMode.INCREMENT_BY_VAL);
			plot.setDomainStepValue(1);

			// thin out domain/range tick labels so they dont overlap each other:
			plot.setTicksPerDomainLabel(5);
			plot.setTicksPerRangeLabel(3);
			plot.disableAllMarkup();
			
			plot.setBorderStyle(BorderStyle.SQUARE, 0f, 0f);
			//plot.setBackgroundPaint(null);
			
			// freeze the range boundaries:
			plot.setRangeBoundaries(0, 1023, BoundaryMode.AUTO);
			plot.setDomainBoundaries(0, 20, BoundaryMode.FIXED);

			// kick off the data generating thread:
			//new Thread(data).start();

		} catch (Exception e) {
			Logger.w(TAG, "Cannot add plot for some reason: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Add a receive listener for a specific channel. It updates the datasource and plot if new data is received.
	 * New data can be sent either in "x:y" format or only "y".
	 * When using setPlotXBoundaries() with boundaryMode = FIXED, the max value there should match the maxSize here 
	 * 
	 * @param channel used for sending data
	 * @param title for this series
	 * @param maxSize maximum amount of data points to display
	 * @param lineColor Either a color word (red, blue etc) or hex value (#FFFFFF)
	 * @param fill Simple line or filled area?
	 * 
	 * FIXME: id muesste gar nicht mit hier, aber wird in getInstance gebraucht und vom dispatcher migeliefert..
	 */
	@Command("2")
	public void addSeries(int id, int channel, String title, int maxSize, String lineColor, boolean fill) {
		
		// Create the series and add it to the plot
		int lineColorValue;
		try {
			lineColorValue = Color.parseColor(lineColor);
		} catch (Exception e) {
			lineColorValue = Color.BLUE;
			Logger.w(TAG, "Series color '" + lineColor + "' is not valid. Use '#FFFFFF' or color names.");
		}
		
		LineAndPointFormatter formatter = new LineAndPointFormatter(lineColorValue, null, fill ? lineColorValue : null);
		
		final Datasource datasource = new Datasource(title);
		datasource.maxSize= maxSize > 0 ? maxSize : 20;
		
        plot.addSeries(datasource, formatter);
        
		// Create a arduino bt receiver for the selected channel
		ArduinoBT.ReceiveListener receiver = new ArduinoBT.ReceiveListener(channel) {			
			
			Plot plot = WidgetPlot.this.plot;
			
			@Override
			public void receive(int key, String command) {				
				String[] tokens = TextUtils.split(command, DELIM_XY);
				Float x = null, y = null;
				
				try {				
					if (tokens.length > 1) {
						x = Float.valueOf(tokens[0]);
						y = Float.valueOf(tokens[1]);						
					} else {
						x = (float) datasource.size(); 
						y = Float.valueOf(tokens[0]);
					}
															
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				datasource.appendData(x, y);
				plot.postInvalidate();				
			}
		};
		
		// Add the data receive listener to our Arduino class.
		manager.getUi().getArduino().addReceiveListener(receiver);
	}
	
	
	/**
	 * Set the plot's x boundaries and boundary mode.
	 * rangeMode = FIXED | AUTO | GROW | SHRINNK
	 * (range values are only used when set to FIXED)
	 * 
	 * @param rangeFrom
	 * @param rangeTo
	 * @param rangeMode
	 */
	@Command("3")
	public void setPlotXBoundaries(int id, int rangeFrom, int rangeTo, String rangeMode) {
		BoundaryMode b = BoundaryMode.valueOf(rangeMode);
		if (b == null) {
			Logger.w(TAG, "setPlotXBoundaries: Invalid rangeMode '" + rangeMode + "'");
			b = BoundaryMode.AUTO;
		}
		plot.setDomainBoundaries(rangeFrom, rangeTo, b);
	}

	/**
	 * Set the plot's y boundaries and boundary mode.
	 * rangeMode = FIXED | AUTO | GROW | SHRINNK
	 * (range values are only used when set to FIXED)
	 * 
	 * @param rangeFrom
	 * @param rangeTo
	 * @param rangeMode
	 */
	@Command("4")
	public void setPlotYBoundaries(int id, int rangeFrom, int rangeTo, String rangeMode) {
		BoundaryMode b = BoundaryMode.valueOf(rangeMode);
		if (b == null) {
			Logger.w(TAG, "setPlotYBoundaries: Invalid rangeMode '" + rangeMode + "'");
			b = BoundaryMode.AUTO;
		}
		plot.setRangeBoundaries(rangeFrom, rangeTo, b);
	}
	
	
	private class Datasource implements XYSeries {
		
		private ArrayList<Number> xValues = new ArrayList<Number>();
		private ArrayList<Number> yValues = new ArrayList<Number>();
		private String title;
		private Integer maxSize;
		
		public Datasource (String title) {
			this.title = title;
		}
 		

		@Override
		public String getTitle() {
			return title;
		}

		@Override
		public int size() {
			return yValues.size();
		}

		@Override
		public Number getX(int pos) {
			return pos;
			//return xValues.get(pos);
		}

		@Override
		public Number getY(int pos) {			
			try {
				return yValues.get(pos);	
			} catch (Exception e) {
				// E.g. if the display was rotated in the wrong moment..
				return 0;
			}
		}
		
		/**
		 * Append multiple data points to this data series
		 * @param x axis data
		 * @param y axis data
		 * @return true if the operation was successful
		 */
		public boolean appendData(List<Number> x, List<Number> y) {
			
			if (xValues.size() != xValues.size()) {
				Logger.w(TAG, "Cannot append plot data: x and y arrays are not the same size!");
				return false;
			}
			
			xValues.addAll(x);
			yValues.addAll(y);
			
			// Resize data arrays if too big
			if (maxSize != null && xValues.size() > maxSize) {
				xValues.subList(0, xValues.size() - maxSize - 1).clear();
				yValues.subList(0, yValues.size() - maxSize - 1).clear();
			}
			
			return true;
		}
		
		/**
		 * Append a data point to this data series
		 * @param x axis data point
		 * @param y axis data point
		 * @return true if the operation was successful
		 */
		public boolean appendData(Number x, Number y) {
			
			if (x == null || y == null) {
				Logger.w(TAG, "Cannot append plot data: x or y is null");
				return false;
			}
			
			xValues.add(x);
			yValues.add(y);
			
			// Resize data arrays if too big
			if (maxSize != null && xValues.size() > maxSize) {
				xValues.subList(0, xValues.size() - maxSize - 1).clear();
				yValues.subList(0, yValues.size() - maxSize - 1).clear();
			}
			
			return true;
		}

		/** 
		 * Remove all data points
		 */
		public void clear() {
			xValues.clear();
			yValues.clear();
		}
		
	}

}
