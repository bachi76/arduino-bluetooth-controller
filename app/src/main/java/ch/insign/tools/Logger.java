/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools;

import android.util.Log;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by bachi on 31.05.15.
 */
public class Logger {

    private final static int LOG_SIZE = 200;

    public static class LogEntry {
        public LogEntry(int level, String tag, String msg) {
            this.level = level;
            this.tag = tag;
            this.msg = msg;
        }
        public int level;
        public String tag;
        public String msg;
    }

    private static LinkedBlockingQueue<LogEntry> log = new LinkedBlockingQueue<>(LOG_SIZE);

    public static LinkedBlockingQueue<LogEntry> getLog() {
        return log;
    }

    synchronized private static void handle(int level, String tag, String msg) {
        if (log.remainingCapacity() < 1) log.remove();
        try {
            log.put(new LogEntry(level, tag, msg));

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static int v (String tag, String msg) {
        handle(Log.VERBOSE, tag, msg);
        return Log.v(tag, msg);
    }
    public static int d (String tag, String msg) {
        handle(Log.DEBUG, tag, msg);
        return Log.d(tag, msg);
    }
    public static int i (String tag, String msg) {
        handle(Log.INFO, tag, msg);
        return Log.i(tag, msg);
    }
    public static int w (String tag, String msg) {
        handle(Log.WARN, tag, msg);
        return Log.w(tag, msg);
    }
    public static int e (String tag, String msg) {
        handle(Log.ERROR, tag, msg);
        return Log.e(tag, msg);
    }

    public static int v (String tag, String msg, Throwable t) {
        handle(Log.VERBOSE, tag, msg);
        return Log.v(tag, msg, t);
    }
    public static int d (String tag, String msg, Throwable t) {
        handle(Log.DEBUG, tag, msg);
        return Log.d(tag, msg, t);
    }
    public static int i (String tag, String msg, Throwable t) {
        handle(Log.INFO, tag, msg);
        return Log.i(tag, msg, t);
    }
    public static int w (String tag, String msg, Throwable t) {
        handle(Log.WARN, tag, msg);
        return Log.w(tag, msg, t);
    }
    public static int e (String tag, String msg, Throwable t) {
        handle(Log.ERROR, tag, msg);
        return Log.e(tag, msg, t);
    }
}
