/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.tools;

import android.content.pm.PackageManager;
import android.util.DisplayMetrics;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ch.insign.arduino.MyApp;

/**
 * A collection of generic tools not worth having their own class.
 * Requires: The 'MyApp' application subclass
 * 
 * @author bachi
 *
 */
public class Tools {

	private static final String TAG = "Tools";

	/**
	 * Find out if an app is installed or not	 
	 * @param uri, e.g. 'ch.comparis.smartshopper'
	 * @return
	 */
	public static boolean isAppInstalled(String uri) {
		PackageManager pm = MyApp.getContext().getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}


	/**
	 * MD5 generation ought to be 1 line of code
	 * @param input
	 * @return
	 */
	public static String md5(String input) {
		try     {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1,messageDigest);
			String md5 = number.toString(16);
			while (md5.length() < 32)
				md5 = "0" + md5;
			return md5;
		} catch(NoSuchAlgorithmException e) {
			return null;
		}
	}

	/** 
	 * Convert from dp to pixel
	 * @param dp
	 * @return pixel equivalent on the current device
	 */
	public static int dpToPx(int dp) {
		DisplayMetrics displayMetrics = MyApp.getContext().getResources().getDisplayMetrics();
		return (int)((dp * displayMetrics.density) + 0.5);
	}

	/** 
	 * Convert from pixel to dp
	 * @param px pixel
	 * @return dp on the current device
	 */
	public static int pxToDp(int px) {
		DisplayMetrics displayMetrics = MyApp.getContext().getResources().getDisplayMetrics();
		return (int) ((px/displayMetrics.density)+0.5);
	}


	public static InputStream getInputStreamFromUrl(String url) throws IOException {
		InputStream content = null;

		HttpClient httpclient = new DefaultHttpClient();
		HttpResponse response = httpclient.execute(new HttpGet(url));
		content = response.getEntity().getContent();

		return content;
	}

	/**
	 * Read an input stream into a string
	 * Note: Any non-unix newlines are converted to \n
	 *
	 * @return content as string
	 * @throws IOException 
	 */
	public static String inputStreamToString(InputStream is) throws IOException {
		if (is == null) {
			Logger.w(TAG, "Input stream was null");
			return "";
		}
		
		String line = "";
		StringBuilder total = new StringBuilder();

		// Wrap a BufferedReader around the InputStream
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));

		// Read response until the end
		
			while ((line = rd.readLine()) != null) { 
				total.append(line);
				total.append("\n");
			}
		
		// Return full string
		return total.toString();
	}

	/**
	 * Get the content of an url as string	
	 * 
	 * @param url
	 * @return content of the url or empty string
	 * @throws IOException 
	 */
	public static String getUrlContent(String url) throws IOException {
		return inputStreamToString(getInputStreamFromUrl(url));
	}


}
