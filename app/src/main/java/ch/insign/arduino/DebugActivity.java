/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.arduino;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import ch.insign.tools.Logger;

/**
 * Shows the log and debug activity.
 */
public class DebugActivity extends Activity {
    private static final String TAG = DebugActivity.class.getSimpleName();

    TextView tvLog;
    RadioGroup radioButtons;
    RadioButton radioArduino;
    RadioButton radioInfo;
    RadioButton radioDebug;

    Thread updater;
    Map<Integer, Integer> colors = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug);

        tvLog = (TextView) findViewById(R.id.debug_log);
        radioButtons = (RadioGroup) findViewById(R.id.debug_radios);
        radioArduino = (RadioButton) findViewById(R.id.debug_level_arduino);
        radioInfo = (RadioButton) findViewById(R.id.debug_level_info);
        radioDebug = (RadioButton) findViewById(R.id.debug_level_debug);

        colors.put(Log.VERBOSE, Color.GRAY);
        colors.put(Log.DEBUG, Color.DKGRAY);
        colors.put(Log.INFO, Color.BLACK);
        colors.put(Log.WARN, Color.BLUE);
        colors.put(Log.ERROR, Color.RED);
        colors.put(99, Color.MAGENTA); // from Arduino

    }

    @Override
    protected void onResume() {
        super.onResume();

        updater = new Thread(new Runnable() {
            @Override
            public void run() {

                while(!Thread.currentThread().isInterrupted()) {
                    try {

                        // Blocks if no entry is available
                        final Logger.LogEntry entry = Logger.getLog().take();

                        // Use a special level / color for log msgs from the Arduino
                        if ("ARDUINO".equals(entry.tag)) entry.level = 99;

                        // Filter messages depending on the chosen level
                        switch (radioButtons.getCheckedRadioButtonId()) {

                            case R.id.debug_level_debug:
                                if (entry.level == Log.VERBOSE) continue;
                                break;

                            case R.id.debug_level_info:
                                if (entry.level == Log.VERBOSE || entry.level == Log.DEBUG) continue;
                                break;

                            case R.id.debug_level_arduino:
                                if (!"ARDUINO".equals(entry.tag)) continue;
                                break;
                        }

                        tvLog.post(new Runnable() {
                            @Override
                            public void run() {

                                appendColoredText(tvLog, entry.msg + "\n",
                                        colors.containsKey(entry.level) ? colors.get(entry.level) : Color.YELLOW);
                            }
                        });

                        if (! Thread.currentThread().isInterrupted()) {
                            Thread.sleep(100);
                        }

                    } catch (InterruptedException e) {
                        break;
                    }

                }
                Logger.d(TAG, "Log update thread interrupted.");
            }
        });

        updater.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        updater.interrupt();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return MyApp.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public static void appendColoredText(TextView tv, String text, int color) {
        int start = tv.getText().length();
        tv.append(text);
        int end = tv.getText().length();

        Spannable spannableText = (Spannable) tv.getText();
        spannableText.setSpan(new ForegroundColorSpan(color), start, end, 0);
    }

    public void btnClear_onClick(View view) {
        tvLog.setText("");
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean("myOption1", radioArduino.isChecked());
        savedInstanceState.putBoolean("myOption2", radioInfo.isChecked());
        savedInstanceState.putBoolean("myOption3", radioDebug.isChecked());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        radioArduino.setChecked(savedInstanceState.getBoolean("myOption1"));
        radioInfo.setChecked(savedInstanceState.getBoolean("myOption2"));
        radioDebug.setChecked(savedInstanceState.getBoolean("myOption3"));
    }
}
