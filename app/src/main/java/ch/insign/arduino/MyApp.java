/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.arduino;

import android.content.Context;
import android.content.Intent;
import android.view.Menu;

/**
 * Application subclass, accessible everywhere.
 */

// TODO: Consider enabling before Launch: @ReportsCrashes(formUri = "http://www.bugsense.com/api/acra?api_key=3c6d601d", formKey="") // now using the Bugsense.com backend
// (that is, if considering there's enough time to act on bugreports, anyway..)
public class MyApp extends android.app.Application {

	private static MyApp instance; 

	public MyApp() {
		super();    	
		instance = this;
	}

	@Override
	public void onCreate() {
		// The following line triggers the initialization of ACRA
		//TODO: ACRA.init(this);
		super.onCreate();
	}
	
	/**
	 * Get the singleton instance of the Application subclass
	 * @return
	 */
	public static MyApp getInstance() {
		return instance;
	}
	
	/**
	 * Get this app's application context
	 * @return The Apps Context
	 */
	public static Context getContext() {
		return instance.getApplicationContext();
	}
	
	/**
	 * Should be called in onCreateOptionsMenu of activities
	 * @param menu The Menu
	 * @return true, so the menu will be displayed.
	 */
	public static boolean onCreateOptionsMenu(final Menu menu) {		

						
		menu.add(instance.getString(R.string.menu_arduinoUi))
		/*.setIcon(R.drawable.ic_action_compare)*/
		.setIntent(new Intent(getContext(), ArduinoActivity.class));
		//.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		menu.add(instance.getString(R.string.menu_commlog))
		/*.setIcon(R.drawable.ic_action_compare)*/
		.setIntent(new Intent(getContext(), DebugActivity.class));
		//.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);

		menu.add(R.string.menu_connect)
		/*.setIcon(R.drawable.ic_action_compare)*/
		.setIntent(new Intent(getContext(), BTSelectionActivity.class));
		
		//.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		return true;
	}


	
	
	
	
}
