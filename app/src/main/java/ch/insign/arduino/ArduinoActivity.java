/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.arduino;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import ch.insign.tools.Logger;
import ch.insign.tools.arduino.ArduinoBT;
import ch.insign.tools.arduino.ui.UI;

/**
 * This activity holds the UI the arduino will send.
 * It also checks if the connection is still alive.
 * TODO: Currently it detects conn probs only when sending. Add a heartbeat mechanism.
 */
public class ArduinoActivity extends Activity {

    private ArduinoBT arduino = ArduinoBT.getInstance();

    private static final String TAG = ArduinoActivity.class.getSimpleName();
    private UI ui;
    private Button connectButton;
    private Thread monitor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arduino);

        connectButton = (Button) findViewById(R.id.arduino_connect);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (TEST_MODE) {
            Toast.makeText(this, "TEST MODE", Toast.LENGTH_SHORT).show();
            ui = new UI(this, R.id.parentLayout);
            ui.connectToArduino(arduino, 1);
            testUI();
            return;
        }

        // If connected, request the UI. If not, show the connect button
        if (arduino.isConnected()) {

            // Start connection monitoring thread
            monitor = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!Thread.currentThread().isInterrupted()) {

                        Log.i(TAG, "Conn: " + arduino.isConnected());

                        if (arduino.isConnected() && connectButton.getVisibility()==View.VISIBLE) {
                            connectButton.post(new Runnable() {
                                @Override
                                public void run() {
                                    connectButton.setVisibility(View.GONE);
                                }
                            });
                        }

                        if (!arduino.isConnected() && connectButton.getVisibility()==View.GONE) {
                            connectButton.post(new Runnable() {
                                @Override
                                public void run() {
                                    connectButton.setVisibility(View.VISIBLE);
                                    Logger.w(TAG, "BT connection lost.");
                                }
                            });
                        }

                        if (!Thread.currentThread().isInterrupted()) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
            monitor.start();

            connectButton.setVisibility(View.GONE);
            ui = new UI(this, R.id.parentLayout);
            ui.connectToArduino(arduino, 1);

        } else {

            connectButton.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (monitor != null) monitor.interrupt();
    }

    public void buttonConnect_onClick(View v) {
        startActivity(new Intent(this, BTSelectionActivity.class));
    }


    // Enable the TEST_MODE to simulate commands from Arduino sent to Android
    private static final boolean TEST_MODE = false;

    private void testUI() {

        // Build a little test ui
//                ui.executeCommand("a1>101>Label of TV>1>30>" + Gravity.CENTER);
//                ui.executeCommand("c2");
//                ui.executeCommand("a1>102>second line>1>20>" + Gravity.RIGHT);
//                // New Row
//                ui.executeCommand("c2");
//        //
//                // Buttons
//                ui.executeCommand("b1>97>A");
//                ui.executeCommand("b1>98>B");
//                ui.executeCommand("b1>99>C");
        //
        //        // Modify button 1
        //        ui.executeCommand("b1>97>XXX");
        //
        //        ui.executeCommand("1");
        //
        //        // Modify TV
        //        ui.executeCommand("a1>101>Label of TV modified>1>30>" + Gravity.CENTER);

        //		// Parameter type test
        //		ui.executeCommand("t1>22>22>33.33>33.33>44>44>Hello!>1>1");


        // ****Plot test
//		ui.executeCommand("p1>1>Plottest>200");
//		ui.executeCommand("p2>1>7>Chan7>100>#D00000>1");
//		ui.executeCommand("p4>1>4>8>FIXED");
//		ui.executeCommand("p3>1>0>100>GROW");
//		ui.executeCommand("c2");
//		ui.executeCommand("p1>2>Plottest2>200");
//		ui.executeCommand("p2>2>7>Chan7>20>blue>0");

        // **** Simulate Cosm client - receive data on channel 7 and forward data to https://cosm.com/feeds/96416
        //int channel, final String feed, final int cosmStreamId, final String apikey
        //ui.executeCommand("w3>7>96416>0>DmoinH_4HwHYojnWCv8kh4sF2TiSAKxTQlNZd0g2K1BXcz0g");

        // **** Simulate Google Spreadsheet client - receive data on channel 7 and forward data to https://cosm.com/feeds/96416
        // public void toGoogleSpreadsheet(int channel, final String formKey, final String fieldKey)
        //ui.executeCommand("w4>7>1nSux8vDKLrlAQ1bhjN_TNuZWmIEnpZA0yJaYGySiKHA>entry.434867443");

        // **** Simulate receiving data point packets on channel 7 every second
//		Timer t = new Timer();
//		t.scheduleAtFixedRate(new TimerTask() {
//
//			@Override
//			public void run() {
//				String val = String.valueOf(Math.random() * 10d);
//				if (ui.getArduino() != null) {
//					ui.getArduino().injectPacket(String.format("7%s%s", ArduinoBT.DELIM_VALUE, val));
//				}
//			}
//		}, 100, 100);
        // long url: https://docs.google.com/spreadsheet/pub?key=0AggNtCexSehYdG52TmwzQTdnM3RXM1JQRFpXNzNFeWc&single=true&gid=2&range=B6&output=txt
        //ui.executeCommand("r2>1>http://goo.gl/xO7bl>10");

        // Get the time
        // ui.executeCommand("r1>1");

        // Get the accelerometer X and Y axis every 500ms, multiply by 2 to get 20 values on each side of each axis (9.81*2)
        // ui.executeCommand("r3>1>X>500>2");
        // ui.executeCommand("r3>2>Y>500>2");

        // public void create(int id, final int min, int max, int initial, int stepSize)
        // New table
        ui.executeCommand("c1>1");
        ui.executeCommand("a1>356>Value 1>1>14>" + Gravity.LEFT);
        ui.executeCommand("c2");
        ui.executeCommand("s1>1>5>15>10>2");

        // Log entry
        ui.executeCommand("w5>We love log entries!");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return MyApp.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
