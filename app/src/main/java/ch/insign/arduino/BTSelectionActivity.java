/*
 * Arduino Bluetooth Controller
 * (c) 2015 Martin Bachmann, m.bachmann@insign.ch
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * @license GPL-3.0+ <http://opensource.org/licenses/GPL-3.0>
 */

package ch.insign.arduino;

import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.security.NoSuchProviderException;
import java.util.Set;

import ch.insign.tools.Logger;
import ch.insign.tools.arduino.ArduinoBT;

/**
 * Shows the list of bonded bluetooth devices and lets you choose one to connect to.
 */
public class BTSelectionActivity extends ListActivity {

    private static final String TAG = BTSelectionActivity.class.getSimpleName();

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        // Does the device have BT capabilities?
        if (btAdapter == null) {
            Toast.makeText(this, "Sorry, your device seems not to have bluetooth capabilities.", Toast.LENGTH_LONG).show();
            //finish();
            return;
        }

        // BT not enabled, try to enable it (interactively)
        if(!btAdapter.isEnabled()) {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, 0);
        }

        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();

        for (BluetoothDevice pairedDevice  : pairedDevices) {
            adapter.add(pairedDevice.getName());
            Logger.i(TAG, pairedDevice.getName());
        }
        adapter.notifyDataSetChanged();
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        String btName = (String) getListAdapter().getItem(position);
        ArduinoBT arduino = ArduinoBT.getInstance();

        try {

            // Disconnect previous device
            arduino.removeAllReceiveListeners();
            if (arduino.isConnected()) {
                arduino.disconnect();
            }

            // Connect new device
            arduino.setup(this, btName);


            // Close chooser activity
            finish();

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Problem occured: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } catch (NoSuchProviderException e) {
            Toast.makeText(this, btName + " was not found.", Toast.LENGTH_LONG).show();
        }
    }

}
