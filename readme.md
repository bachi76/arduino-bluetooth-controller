# Arduino Controller
*The wireless mobile controller that you code right from your Arduino sketch!*

![arduino_snake_800.jpg](https://bitbucket.org/repo/roeabq/images/2315059176-arduino_snake_800.jpg)

The Arduino Controller lets you control your Arduino project from your Android phone or tablet.
You can code interactive UIs using buttons, text fieds, sliders and even live diagrams - right in 
your Arduino sketch! Or you can use your Android's gyroscope to control your Arduino bot. And much more.

See it in action:
[A little Snake demo](https://www.youtube.com/watch?v=388fcW-oD_A) - ([code](https://goo.gl/aayydz)) / [Buttons and a sensor](http://www.insign.ch/blog/projekt/arduino-android-remote-user-interface/)

## Getting started

The project consists of two parts:
* The Android app
* The Arduino library

1. [Install](https://play.google.com/store/apps/details?id=ch.insign.arduino) the Android app (or compile it from the source)
2. Add the Arduino library from _<project>/arduino-code/AndroidController.zip_ to your Arduino library folder
3. Connect a bluetooth serial module (like the JY-MCU), to TX on pin 10 and RX on pin 11
4. Run the provided example program bundled with the library (AndroidControllerDemo)
5. Start the android app and connect to your (previously bonded) bt device

### Bluetooth connection
You will need a module that supports serial transfer (more specifically, the SPP profile). I use the JY-MCU bluetooth module in my project, which costs around $6 and [can be ordered e.g. here](http://www.dx.com/p/jy-mcu-arduino-bluetooth-wireless-serial-port-module-104299#.VXhPbvvtlBc).

Note: There's no bluetooth 4 (BLE) support yet, as BLE works in a fundamentally different way. Which also means, that unfortunately the RFduino does not work at the moment (contributors welcome).

## Example

The example bundled with the library should show you how the library works. It will:
- Measure analog input on pin A0
- Send the measured value as text field
- Create a live plot that shows the measured value over time
- Add a slider to regulate the measuring speed
- Create a button that switches on / off blinking of the internal (depending on the Arduino) LED on pin 13.

    :::c
    #include <android_controller.h>
	#include <SoftwareSerial.h>


	// Define a subclass of AndroidController
	class MyAndroidController : public AndroidController {

		public:

		// Ids for UI elements
		static const int LBL_TITLE = 0;
		static const int LBL_SENSOR_VALUE = 1;
		static const int TXT_SENSOR_VALUE = 2;
		static const int BTN_SWITCH = 3;
		static const int PLOT = 4;
		static const int SLIDER = 5;
		static const int LBL_SLIDER = 6;

		// A data channel where we send sensor data
		static const int  DATA_CHANNEL = 7;

		boolean led = false;
		int speed = 8;

		// Here we define our user interface. Called when an Android client connects and requests the UI.
		void onSetupUI() {

		    Serial.println(F("Sending UI"));
		    resetUI();
		    addText(LBL_TITLE, F("Demo application"), 1, 36, GRAVITY_LEFT);        
		    addNewRow();
		            
		    addText(LBL_SENSOR_VALUE, F("Sensor 1: "), 1, 24, GRAVITY_LEFT);
		    addText(TXT_SENSOR_VALUE, F("--"), 1, 24, GRAVITY_RIGHT);
		    bindText(TXT_SENSOR_VALUE,DATA_CHANNEL);
		    
		    addNewRow();
		    addLinePlot(PLOT, F("Sensor data plot"), 200);
		    addPlotSeries(PLOT, DATA_CHANNEL, F("A0 analog input"), 20, "red", true);
		    // Set optional plot boundaries (default is auto-scale).
		    // setPlotYBoundaries(PLOT, 200, 500, "FIXED");

		    addNewRow();                        
		    addButton(BTN_SWITCH, getLEDLabel());
		    
		    addNewRow();
		    addText(LBL_SLIDER, F("Measuring speed"), 1, 24, GRAVITY_LEFT);
		    addNewRow();
		    addSlider(SLIDER, 1, 10, speed, 1);                
		}

		// Callback to handle button clicks
		void onButtonClick(int id) {
		    switch (id) {

		        case BTN_SWITCH:
		            led = !led;
		            
		            // To update a button label, just issue 'addButton' with the same id again
		            addButton(BTN_SWITCH, getLEDLabel());                
		            break;
		    }
		}
		
		// Callback to read slider changes
		void onSliderChange(int id, int value) {
		     switch(id) {
		        case SLIDER:
		          speed = value;
		          break;
		     } 
		}
		
		String getLEDLabel() {
		    return led ? F("Turn LED off") : F("Turn LED on"); 
		}
	};

	// Create an instance of your AndroidController subclass
	MyAndroidController android;

	boolean ledBlinkState = true;

	void setup() {
		pinMode(13, OUTPUT);
		Serial.begin(9600);

		// Initialize and start the android controller
		android.begin();
	}

	void loop() {

		// Read sensor data from A0
		int m = analogRead(A0);

		// Send sensor data on our data channel (as String).
		android.send(android.DATA_CHANNEL, String(m));

		// If the LED is enabled from the controller, blink it
		if (android.led || ledBlinkState) {
		     ledBlinkState = !ledBlinkState;
		     digitalWrite(13, ledBlinkState ? HIGH : LOW);
		}

		// Need to call this periodically to retrieve data from the Android device
		android.receive();

		// speed is bound to the slider and its value between 1 and 10
		delay(100 * (10 - android.speed));
	}

And this is what the resulting controller should look like:

![arduino_android_controller_demo_ui.png](https://bitbucket.org/repo/roeabq/images/2076946518-arduino_android_controller_demo_ui.png)

### Arduino library API documentation

The full API doc is bundled with the arduino library:

**arduino-code/AndroidController/extras/apidoc/classAndroidController.html**

### UI layout

The UI is arranged in a table-like layout (Android's TableLayout is used). Each widet (buttons, texts, ...) you
add to the same row will create a new column. You are free to add more than one table to your UI.
For vertical spacing, add empty rows.


### Widgets

You fill your UI with widgets like text views, buttons and sliders.

Widgets have ids that can be used to modify them later. In general, if you issue an _add_ command
on an existing id, the existing widget will be updated, eg. for changing colors (for text views there's
 an updateText() function).

### Callbacks

Events in the Android app trigger a callback or event handler on the Arduino side. Overwrite the desired
callback handlers to receive them: onButtonClick, onSliderChange, onReadValue. The event's origin
(e.g. which button) is passed in as widget id or request id, which you can handle in a switch/case
statement as shown in the example.

### Channels

If you process regular input data (such as periodical analog measurements), you can use a _channel_
to send the data to the phone. Using channels, you can bind multiple widgets and write commands to
the same data, which is still only sent once over air. In the above example, we send the measured value
from A0 over a channel and then bind a text widget and a plot series to this channel. We could also output
the valeus to a Google spreadsheet, for example.

### Limitations

#### Memory considerations
Unfortunately, the library is quite memory-hungry (relative to the 2kb RAM of your typical Arduino..),
and creating UIs invites to add more strings here and there. Watch your memory consumption. Use the
F() construct to have strings stored in progmem. Use the freeRam() method to spot issues.

#### Transfer speed
Currently, SoftSerial is used, which maxes at 9'600kbps. The library has potential for optimisation,
(e.g. allow to optionally use the hw serial interface) but hasn't been an issue for my projects yet.

#### Transfer protocol
The transfer protocol is quite primitive, everything is transferred as string and the used delimiters are
 not even escaped when used in data yet. Contributors welcome, I leave this field for more experienced C hackers.


## Getting involved

Looking for developers who would like to get involved and enhance or optimize the existing code. Please
get in contact. Pull Requests and active contributors are most welcome.

## License

Arduino Bluetooth Controller
(c) 2015 Martin Bachmann, m.bachmann 'at' insign 'dot' ch

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 3 as published by the Free Software Foundation
<http://opensource.org/licenses/GPL-3.0>