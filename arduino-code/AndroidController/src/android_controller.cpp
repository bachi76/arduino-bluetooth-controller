/* Include the software serial port library */
#include <SoftwareSerial.h>
#include "android_controller.h"

/* Default IO pin used to communicate with the Bluetooth module's TXD pin. */
#define BT_SERIAL_TX_DIO 10 
/* Default IO pin used to communicate with the Bluetooth module's RXD pin */
#define BT_SERIAL_RX_DIO 11

SoftwareSerial *BluetoothSerial = NULL;

/* Constructor */
AndroidController::AndroidController() {	

	// Delimiters (don't use const here as it uses string, not String ..)
	DELIM_VALUE = "~"; 
	DELIM_PACKET = "|";
	PARAM_DELIM = ">";
		
	// Others
	receiveBuffer.reserve(30);
	receiveBuffer = "";
	commandBuffer.reserve(30);
	debugMode = true;			
}

void AndroidController::begin() {	
	begin(BT_SERIAL_TX_DIO, BT_SERIAL_RX_DIO);
}

void AndroidController::begin(int pinTx, int pinRx) {
	BluetoothSerial = new SoftwareSerial(pinTx, pinRx);
	BluetoothSerial->begin(9600);
}

/** Sets the callback to be called when receiving comamnds. Set a pointer to a func(int channel, String command) 
	Note: Deprecated, better overwrite the onIncomingCommand() in your subclass **/
void AndroidController::setReceiveCallback(receiveCallback f) {
	recCallback = f;
}

/** Send data over BT to the Android device. Use a target channel. Sends the internal command buffer **/
void AndroidController::send(int channel) {

	/*Serial.print("Sending: ");
	Serial.print((String) channel);
	Serial.print(DELIM_VALUE);
	Serial.print(this->commandBuffer);
	Serial.print(DELIM_PACKET);*/
	
	BluetoothSerial->print((String) channel);
	BluetoothSerial->print(DELIM_VALUE);
	BluetoothSerial->print(this->commandBuffer);
	BluetoothSerial->print(DELIM_PACKET);

	//Serial.print(F("free mem: "));
	//Serial.println(freeRam());	
	delay(20);
}

/** Send data over BT to the Android device. Use a target channel. **/
void AndroidController::send(int channel, String command) {
	this->commandBuffer = command;
	send(channel);
}

/** Try to receive BT data from the Android device. Incoming data will be buffered and split into commands **/
void AndroidController::receive() {
  // TODO: return the latest value of that source. Buffer any other. 
  if (BluetoothSerial->available()) {	
	String data;
	data.reserve(30);
	while(BluetoothSerial->available()) {
	  data += char(BluetoothSerial->read());
	}

	//debug("Received raw data: " + data + " Buffer: " + receiveBuffer);

	// Add previous bytes
	data = receiveBuffer + data;
	//debug("Buffer+Data: " + data);

	// Decode the incoming data stream into channels and commands.
	// Raw data example: xyz|n2~Hallo|n3~World|n2~cu
	// In this example, 2~cu is not complete (missing \n), thus it's added
	// to the receiveBuffer and added in front of the next incoming raw data stream
	while (data.indexOf("|") > -1) {

	  // Take a data packet from the stream, cut it from the remaining data stream
	  String packet = data.substring(0, data.indexOf("|")); 
	  data = data.substring(data.indexOf("|") + 1);

	  // Split the packet into a channel and a command, e.g.:
	  // 2~Hello -> channel=2, command=Hello
	  // Note: The delim cannot be at the beginning or end of the package
	  
	  // debug ("Packet:" + packet); 
	  unsigned int pos = packet.indexOf("~");

	  if (pos > 0 && pos < packet.length()) {
		String channelStr = packet.substring(0, pos);
		int channel = channelStr.toInt();
		commandBuffer = packet.substring(pos + 1);

		//// debug("receive: channel " + channelStr + ": command: " + command);
		
		// Execute the received command
		executeIncomingCommand(channel, commandBuffer);
		
	  } 
	  else {
		///// debug("receive: Invalid data packet: " + data);
	  }      
	}
	// Add remaining data (after the last packet delim) to the receiveBuffer
	receiveBuffer = data;
  }
  
}

void AndroidController::debug(String msg) {
  if (debugMode) Serial.println(msg);
}

void AndroidController::executeIncomingCommand(int channel, String command) {
			
	// Tokenize a command string (split by a delimiter char and return a string array).	
	String tokens[10];		
	tokenize(command, PARAM_DELIM, tokens);
		
	// UI channel
	if (channel == 1) {
	
		// UI request		
		if (tokens[0].equals(REMOTE_CMD_NEED_UI)) {
			///// debug("UI requested");
			onSetupUI();
		}
		
		// Button click
		if (tokens[0].equals(REMOTE_CMD_BUTTON_PRESSED)) {
			int buttonId = tokens[1].toInt();
			debug("Button " + String(buttonId) + " pressed.");
			onButtonClick(buttonId);
		}
		
		// Requested read value returned
		if (tokens[0].equals(REMOTE_CMD_READ)) {
			int requestId = tokens[1].toInt();
			onReadValue(requestId, tokens[2]);
		}
		
		// A slider was moved
		if (tokens[0].equals(REMOTE_CMD_SLIDER_CHANGED)) {
			debug("Slider " + tokens[1] +": " + tokens[2]);
			onSliderChange(tokens[1].toInt(), tokens[2].toInt());
		}
		
	}
	
	// If we have a callback, call it now (deprecated)
	if (recCallback) {
		(*recCallback)(channel, command);
	}

	// Call the handler (preferred)
	onIncomingCommand(channel, command);
}

void AndroidController::onIncomingCommand(int channel, String command) {}

void AndroidController::onButtonClick(int id) {}

void AndroidController::onSliderChange(int requestId, int value) {}

void AndroidController::onReadValue(int requestId, String value) {}

int AndroidController::tokenize(String data, String delim, String tokenArray[]) {
	int MAX_TOKENS = 10;		
	int numArgs = 0;
	int posFrom = 0;
	int posTo = data.indexOf(delim);	
	while (posTo != -1 && numArgs < MAX_TOKENS-1) {	
		//Serial.println("Adding '" + data.substring(posFrom, posTo) + "' ...");
		tokenArray[numArgs++] = data.substring(posFrom, posTo);				
		//Serial.println(String(posFrom) + " to " + String(posTo) + " = token " + String(numArgs-1) + ": " + tokenArray[numArgs-1]);
		posFrom = posTo + delim.length();
		posTo = data.indexOf(delim, posFrom);
	}
	tokenArray[numArgs++] = data.substring(posFrom);
	//Serial.println("Adding '" + data.substring(posFrom) + "' ...");
	return numArgs;
}

void AndroidController::onSetupUI() {
	resetUI();	
}
	
void AndroidController::addButton(int id, String label) {

	this->commandBuffer =  
		CMD_ADD_BUTTON + PARAM_DELIM + 
		String(id) + PARAM_DELIM + 
		label;

  send(1);
}

void AndroidController::addSlider(int id, int min, int max, int initial, int stepSize) {
	
	this->commandBuffer =  
		CMD_ADD_SLIDER + PARAM_DELIM + 
		String(id) + PARAM_DELIM + 
		String(min) + PARAM_DELIM +
		String(max) + PARAM_DELIM +
		String(initial) + PARAM_DELIM +
		String(stepSize);

	send(1);
}

void AndroidController::setSlider(int id, int value) {
	
	this->commandBuffer =  
		CMD_SET_SLIDER + PARAM_DELIM + 
		String(id) + PARAM_DELIM + 
		String(value) ;

	send(1);
}

void AndroidController::addText(int id, String label, int maxLines, int textSize, int gravity) {

  	this->commandBuffer =   
		CMD_ADD_TEXT + PARAM_DELIM + 
		String(id) + PARAM_DELIM + 
		label + PARAM_DELIM +
		String(maxLines) + PARAM_DELIM +
		String(textSize) + PARAM_DELIM +
		String(gravity);

	send(1);
}

void AndroidController::updateText(int id, String label) {
	
	this->commandBuffer =   
		CMD_UPDATE_TEXT + PARAM_DELIM + 
		String(id) + PARAM_DELIM +
		label;
		
	send(1);
}

void AndroidController::bindText(int id, int channel) {
	
	this->commandBuffer =   
		CMD_BIND_TEXT + PARAM_DELIM + 
		String(id) + PARAM_DELIM +
		String(channel);
		
	send(1);
}

void AndroidController::addLinePlot(int id, String label, int dpHeight) {

  this->commandBuffer =   
    CMD_ADD_LINE_PLOT + PARAM_DELIM + 
    String(id) + PARAM_DELIM + 
    label + PARAM_DELIM +
    String(dpHeight) ;

  send(1);
}

void AndroidController::addPlotSeries(int id, int channel, String title, int maxSize, String lineColor, boolean fill) {

  this->commandBuffer =   
    CMD_ADD_PLOT_SERIES + PARAM_DELIM + 
    String(id) + PARAM_DELIM + 
    String(channel) + PARAM_DELIM + 
    title + PARAM_DELIM  +
    String(maxSize) + PARAM_DELIM + 
    lineColor + PARAM_DELIM + 
    (fill==true ? "1" : "0");

  send(1);
}

void AndroidController::setPlotXBoundaries(int id, int rangeFrom, int rangeTo, String rangeMode) {
	
	this->commandBuffer =
		CMD_SET_PLOT_X_BOUNDARIES + PARAM_DELIM + 
		String(id) + PARAM_DELIM + 
		String(rangeFrom) + PARAM_DELIM + 
		String(rangeTo) + PARAM_DELIM + 
		rangeMode;
	
	send(1);
}

void AndroidController::setPlotYBoundaries(int id, int rangeFrom, int rangeTo, String rangeMode) {
	
	this->commandBuffer =
		CMD_SET_PLOT_Y_BOUNDARIES + PARAM_DELIM + 
		String(id) + PARAM_DELIM + 
		String(rangeFrom) + PARAM_DELIM + 
		String(rangeTo) + PARAM_DELIM + 
		rangeMode;
	
	send(1);
}

void AndroidController::addNewRow() {
  this->commandBuffer =   
	CMD_ADD_ROW;

  send(1);
}

void AndroidController::addNewTable(boolean stretchable) {
  	this->commandBuffer =  
		CMD_ADD_TABLE + PARAM_DELIM +
		(stretchable==true ? "1" : "0");

  send(1);
}

void AndroidController::resetUI() {
  this->commandBuffer =   
	CMD_RESET_UI;

  send(1);
}

void AndroidController::readValue(int whatToRead, int requestId) {

	this->commandBuffer =  
		REMOTE_CMD_READ + String(whatToRead) + PARAM_DELIM +
		String(requestId);

  send(1);
}

void AndroidController::readUrl(int responseId, String url, int updatePeriod) {
	
	this->commandBuffer =  
		REMOTE_CMD_READ + String(READ_URL) + PARAM_DELIM +
		String(responseId) + PARAM_DELIM +
		url + PARAM_DELIM + 
		String(updatePeriod);
		
	send(1);
}

void AndroidController::readAcceleratometer(int responseId, String axis, int updatePeriod, int multiplier) {

	this->commandBuffer =
		REMOTE_CMD_READ + String(READ_ACCELERATOR) + PARAM_DELIM +
		String(responseId) + PARAM_DELIM +
		axis + PARAM_DELIM +
		String(updatePeriod) + PARAM_DELIM +
		multiplier;

	send(1);
}

void AndroidController::addCosmOutput(int channel, String feed, int cosmStreamId, String apikey) {

  this->commandBuffer =   
    CMD_ADD_COSM_OUTPUT + PARAM_DELIM + 
    String(channel) + PARAM_DELIM + 
    feed + PARAM_DELIM +
    String(cosmStreamId) + PARAM_DELIM + 
    apikey;    

	send(1);
}

void AndroidController::addGoogleSpreadsheetOutput(int channel, String formKey, String fieldKey) {

  this->commandBuffer =   
    CMD_ADD_GOOGLE_SPREADSHEET_OUTPUT + PARAM_DELIM + 
    String(channel) + PARAM_DELIM + 
    formKey + PARAM_DELIM +    
    fieldKey;    

	send(1);
}

void AndroidController::log(String message) {

  this->commandBuffer =   
    CMD_ADD_LOG + PARAM_DELIM + 
    String(message);

	send(1);
}

int AndroidController::freeRam () {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}





