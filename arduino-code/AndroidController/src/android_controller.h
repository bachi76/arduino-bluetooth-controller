//-------------------------------------------------------------------
#ifndef __android_controller_library_main_H__
#define __android_controller_library_main_H__
//-------------------------------------------------------------------
 
#include <Arduino.h>

// Callback definitions
typedef void (*receiveCallback)(int, String);

// Command ids
// (ideally they should be private members but that clutters the code: http://stackoverflow.com/questions/3722775/can-i-declare-a-string-in-a-header-file-in-a-definition-of-a-class)

static const String CMD_ADD_TABLE = "c1";
static const String CMD_ADD_ROW = "c2";
static const String CMD_RESET_UI = "c3";

static const String CMD_ADD_BUTTON = "b1";

static const String CMD_ADD_TEXT = "a1";
static const String CMD_UPDATE_TEXT = "a2";
static const String CMD_BIND_TEXT = "a3";

static const String CMD_ADD_LINE_PLOT = "p1";
static const String CMD_ADD_PLOT_SERIES = "p2";
static const String CMD_SET_PLOT_X_BOUNDARIES = "p3";
static const String CMD_SET_PLOT_Y_BOUNDARIES = "p4";

static const String CMD_ADD_FILE_OUTPUT = "w1";
static const String CMD_ADD_URL_OUTPUT = "w2";
static const String CMD_ADD_COSM_OUTPUT = "w3";
static const String CMD_ADD_GOOGLE_SPREADSHEET_OUTPUT = "w4";
static const String CMD_ADD_LOG="w5";

static const String CMD_ADD_SLIDER = "s1";
static const String	CMD_SET_SLIDER = "s2";


// UI commands from Android 
// (don't use 0)

static const String REMOTE_CMD_NEED_UI = "c1";
static const String REMOTE_CMD_BUTTON_PRESSED = "b1";
static const String REMOTE_CMD_SLIDER_CHANGED = "s1";
static const String REMOTE_CMD_READ = "r";

// WidgetRead params - used both on request and onRead() callback

static const int READ_TIME = 1;
static const int READ_URL = 2;
static const int READ_ACCELERATOR = 3; // TODO: Does this work? Doc it or remove it.


// Android Gravity constants

static const int GRAVITY_LEFT = 3;
static const int GRAVITY_RIGHT = 5;
static const int GRAVITY_CENTER = 17;


/**
 * Create a control UI on your Android device, right from your Arduino.
 * Usage: Subclass AndroidController in your code. 
 */
class AndroidController {
	public:
		AndroidController();
		
		/**
		 * Start the bluetooth connection.
		 * Call this in your setup() code.
		 * Uses default software serial pins: TX = 10, RX = 11
		 */
		void begin();

		/**
		 * Start the bluetooth connection.
		 * Call this in your setup() code.
		 * 
		 * @param pinTx The soft serial tx pin
		 * @param pin Rx The soft serial rx pin
		 */
		void begin(int pinTx, int pinRx);
		

		/**
		 * Handler: UI Request
		 * The android device calls for the ui, e.g. upon a new bluetooth connection.
		 * Overwrite this to define your UI.
		 */
		virtual void onSetupUI();

		/**
		 * Handler: Incoming commands
		 * Used mostly internally - use this for low-level / custom commands.
		 * UI requests are handled before this handler is called.
		 */
		virtual void onIncomingCommand(int channel, String command);

		/**
		 * Handler: A button was clicked on the android device
		 * Overwrite in your subclass to handle button clicks.
		 * 
		 * @param id The id of the button clicked
		 */
		virtual void onButtonClick(int id);

		/**
		 * Handler: A requested read value is being returned
		 * Overwrite in your subclass to handle incoming data.
		 * 
		 * @param requestId The id used in the initial request
		 * @param value the value returned from the Android device
		 */
		virtual void onReadValue(int requestId, String value);

		// TODO: virtual void onReadValueChange(int id, String value);
		
		/**
		 * Handler: A slider value has changed
		 * Overwrite in your subclass to handle slider changes.
		 * 
		 * @param id The id of the slider
		 * @param value The new slider value
		 */
		virtual void onSliderChange(int id, int value);

		/**
		 * Reset the UI. The device will immediately answer with an onSetupUI() request
		 */
		void resetUI();		

		/**
		 * Add a new layout table with fixed or stretchable cells.
		 * Makes the table's columns stretchable or not. When stretchable, a column takes up as much as available space as possible in its row.
		 * 
		 * @param stretchable
		 */
		void addNewTable(boolean stretchable);

		/**
		 * Add a new row to the current layout table.
		 */
		void addNewRow();

		/**
		 * Add a text view.
		 * 
		 * @param id The view's id
		 * @param label The text to be shown
		 * @param maxLines The max # of lines. Default = 1.
		 * @param textSize The size of the text (in Android's SP units). Default text sizes: 14 = small, 18 = large, 22 = large
		 * @param gravity How to place the text in the view. GRAVITY_LEFT, GRAVITY_RIGHT or GRAVITY_CENTER
		 */
		void addText(int id, String label, int maxLines, int textSize, int gravity);

		/**
		 * Update an existing text view.
		 * 
		 * @param id The view's id
		 * @param label The new text label to show
		 */
		void updateText(int id, String label);

		/**
		 * Bind a text view to a channel, so you can just send data on that channel
		 * and the text view is automatically updated (useful if you have multiple widgets
		 * updated on the same data.
		 * 
		 * @param id The view's id
		 * @param channel The channel the text view should listen to.
		 */
		void bindText(int id, int channel);

		/**
		 * Add a button.
		 * 
		 * @param id The button's id
		 * @param label The button's label
		 */
		void addButton(int id, String label);
		
		/**
		 * Add a slider.
		 * 
		 * @param id The slider's id
		 * @param min The slider min value
		 * @param max The slider max value
		 * @param initial The slider's initial value
		 * @param stepSize The increments/decrements per slider step
		 */
		void addSlider(int id, int min, int max, int initial, int stepSize);
		
		/**
		 * Set / update the value of an existing slider.
		 * 
		 * @param id The slider's id
		 * @param value The new value
		 */
		void setSlider(int id, int value);

		/**
		 * Add or update a plot.
		 * 
		 * @param id The plot's id
		 * @param label A label displayed on top of the plot
		 * @param dpHeight The height in Android's density independent 'dp' unit. Use 0 to use all space.
		 */
		void addLinePlot(int id, String label, int dpHeight);

		/**
		 * Add a plot series using a receive listener for a specific channel. It updates the datasource and plot if new data is received.
		 * New data can be sent either in "x:y" format or only "y".
		 * When using setPlotXBoundaries() with boundaryMode = FIXED, the max value there should match the maxSize here 
		 * 
		 * @param channel The channel used for sending data
		 * @param title The series' title
		 * @param maxSize The maximum amount of data points to display
		 * @param lineColor Either a color word (red, blue etc) or hex value (#FFFFFF)
		 * @param fill Draw a simple line (false) or a filled area (true)?
		*/
		void addPlotSeries(int id, int channel, String title, int maxSize, String lineColor, boolean fill) ;
		
		/**
		 * Set the plot's x boundaries and boundary mode.
		 * rangeMode = FIXED | AUTO | GROW | SHRINK
		 * (range values are only used when set to FIXED)
		 * 
		 * @param rangeFrom
		 * @param rangeTo
		 * @param rangeMode One of FIXED | AUTO | GROW | SHRINK
		 */
		void setPlotXBoundaries(int id, int rangeFrom, int rangeTo, String rangeMode);

		/**
		 * Set the plot's y boundaries and boundary mode.
		 * rangeMode = FIXED | AUTO | GROW | SHRINK
		 * (range values are only used when set to FIXED)
		 * 
		 * @param rangeFrom
		 * @param rangeTo
		 * @param rangeMode One of FIXED | AUTO | GROW | SHRINK
		 */
		void setPlotYBoundaries(int id, int rangeFrom, int rangeTo, String rangeMode);

		/**
		 * Send output to the cosm.org service (which was recently renamed)
		 * 
		 * Note: The Cosm service seems to have ceased to exist or was renamed.
		 * So this is probably deprecated.
		 */
		void addCosmOutput(int channel, String feed, int cosmStreamId, String apikey);

		/**
		 * Send output to a Google spreadsheet. Note: Data is sent through a (public) Google 
		 * Form for the given spreadsheet. You need to create one for your spreadsheet first.
		 * 
		 * @param channel The channel to listen for data.
		 * @param formKey The formKey GET parameter of the Google form's url
		 * @param fieldKey The form's field key to send data to (TODO: explain)
		 */
		void addGoogleSpreadsheetOutput(int channel, String formKey, String fieldKey);
		
		/**
		 * Send a log message to the Android device.
		 * It will be visible in the Android app's Log activity, in pink.
		 * It will also be logged to the standard Android logs with log level INFO and log tag "ARDUINO".
		 *  
		 * @param message The message to send. 
		 */
		void log(String message);

		/**
		 * Flush (send) all data in the current send buffer.
		 */
		void send(int channel);

		/**
		 * Send a custom command on the given channel.
		 * (useful for development of the Android app)
		 */
		void send(int channel, String command);

		/**
		 * Read some value from the phone, e.g. the current date/time.
		 * The response will be sent to the onReadValue handler.
		 * Supports: READ_TIME | READ_URL | READ_ACCELERATOR
		 * 
		 * @param whatToRead One of the READ_* constants, defining what we want to read
		 * @param requestId An id which is passed back to the callback handler to identify the response.
		 */
		void readValue(int whatToRead, int requestId);

		/**
		 * Fetch a url and return its content.
		 * Run once (updatePeriod=0) or every [updatePeriod] seconds
		 * Return values are only sent if they have changed.
		 * 
		 * Hint: 
		 * Use a Google Spreadsheet and its 'publish' feature for easy web-config values. 
		 * Create an url for each config value (use the range parameter in GDrive).
		 * 
		 * @param responseId The reponse id to identify this request
		 * @param url The url to fetch
		 * @param updatePeriod The update period in seconds
		 */
		void readUrl(int responseId, String url, int updatePeriod);

		/**
		 * Read the Android device's accelerator senser values.
		 * Run once (updatePeriod=0) or every [updatePeriod] milliseconds
		 *
		 * Hint: Sensor values are roughly between -10 and +10 (earth's G), return is rounded. Use the multiplier to scale
		 *
		 * @param responseId
		 * @param axis X, Y or Z axis
		 * @param updatePeriod in millis
		 * @param multiplier used to multiply sensor value before it it rounded to int.
		 */
		void readAcceleratometer(int responseId, String axis, int updatePeriod, int multiplier);


		/**
		 * Parses incoming comands and dispatches event handler calls.
		 * Could be overriden for adding custom functionality.
		 */
		void executeIncomingCommand(int channel, String command);


		/**
		 * Receives and processes data arriving from the Bluetooth interface.
		 * Call this method regularily in your code (ie. every 50ms inside loop()).
		 */
		void receive();
		
		/**
		 * Debug helper, outputs over usb serial if debugMode=true
		 */
		void debug(String msg);

		void setReceiveCallback(receiveCallback f);

		/**
		 * Tokenizes a data string into the passed array. Max tokens = 10
		 */
		int tokenize(String data, String delim, String tokenArray[]);
			
		int freeRam();
		
	private:
		String DELIM_VALUE;
		String DELIM_PACKET;
		String PARAM_DELIM;
		String receiveBuffer;
		String commandBuffer;
		boolean debugMode;	
		int widgetMaxId;
		
		// UI creation functions (to Android)				

		receiveCallback recCallback;				
};
 
 
//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
 
