var searchData=
[
  ['addbutton',['addButton',['../classAndroidController.html#a96f31bba9ffa8aa74764802e64f66fd1',1,'AndroidController']]],
  ['addcosmoutput',['addCosmOutput',['../classAndroidController.html#a1aa807a5413bd80d6e0e24b26bc85ed3',1,'AndroidController']]],
  ['addgooglespreadsheetoutput',['addGoogleSpreadsheetOutput',['../classAndroidController.html#aded292b6fcf6c08e4aad7d33d6f2ca53',1,'AndroidController']]],
  ['addlineplot',['addLinePlot',['../classAndroidController.html#a12154f67cf17cfcca42655ab7facd554',1,'AndroidController']]],
  ['addnewrow',['addNewRow',['../classAndroidController.html#a987b963f75bb03694120db9891388f4c',1,'AndroidController']]],
  ['addnewtable',['addNewTable',['../classAndroidController.html#a8434ba8833c5b80774bba7074eed0abe',1,'AndroidController']]],
  ['addplotseries',['addPlotSeries',['../classAndroidController.html#a420ed9e1cc5fb568f8fd080345c3c28f',1,'AndroidController']]],
  ['addslider',['addSlider',['../classAndroidController.html#ace3d592aa92bc78e42c70312765bf49b',1,'AndroidController']]],
  ['addtext',['addText',['../classAndroidController.html#aca97ae18540a3185ef15165c6773b8b5',1,'AndroidController']]]
];
