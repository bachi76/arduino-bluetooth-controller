/*
  Android Controller demo

  This demo shows some capabilities of the android controller library. It should render this functional UI
  on your Android device: http://tinyurl.com/naoswqf - and live plot analog input data of A0.
       
  Note: If you add a real sensor to A0 you'll get a more useful plot output.
       
  Requirements:
  * A bluetooth serial module like the JY-MCU, connected to TX on pin 10 and RX on pin 11
  * The android controller app: https://play.google.com/store/apps/details?id=ch.insign.arduino

  Created 14.06.2015
  Martin Bachmann

  https://bitbucket.org/bachi76/arduino-bluetooth-controller/
  
*/

#include <android_controller.h>
#include <SoftwareSerial.h>


// Define a subclass of AndroidController
class MyAndroidController : public AndroidController {

    public:

    // Ids for UI elements
    static const int LBL_TITLE = 0;
    static const int LBL_SENSOR_VALUE = 1;
    static const int TXT_SENSOR_VALUE = 2;
    static const int BTN_SWITCH = 3;
    static const int PLOT = 4;
    static const int SLIDER = 5;
    static const int LBL_SLIDER = 6;

    // A data channel where we send sensor data
    static const int  DATA_CHANNEL = 7;

    boolean led = false;
    int speed = 8;

    // Here we define our user interface. Called when an Android client connects and requests the UI.
    void onSetupUI() {

        Serial.println(F("Sending UI"));
        resetUI();
        addText(LBL_TITLE, F("Demo application"), 1, 36, GRAVITY_LEFT);        
        addNewRow();
                
        addText(LBL_SENSOR_VALUE, F("Sensor 1: "), 1, 24, GRAVITY_LEFT);
        addText(TXT_SENSOR_VALUE, F("--"), 1, 24, GRAVITY_RIGHT);
        bindText(TXT_SENSOR_VALUE,DATA_CHANNEL);
        
        addNewRow();
        addLinePlot(PLOT, F("Sensor data plot"), 200);
        addPlotSeries(PLOT, DATA_CHANNEL, F("A0 analog input"), 20, "red", true);
        // Set optional plot boundaries (default is auto-scale).
        // setPlotYBoundaries(PLOT, 200, 500, "FIXED");

        addNewRow();                        
        addButton(BTN_SWITCH, getLEDLabel());
        
        addNewRow();
        addText(LBL_SLIDER, F("Measuring speed"), 1, 24, GRAVITY_LEFT);
        addNewRow();
        addSlider(SLIDER, 1, 10, speed, 1);                
    }

    // Callback to handle button clicks
    void onButtonClick(int id) {
        switch (id) {

            case BTN_SWITCH:
                led = !led;
                
                // To update a button label, just issue 'addButton' with the same id again
                addButton(BTN_SWITCH, getLEDLabel());                
                break;
        }
    }
    
    // Callback to read slider changes
    void onSliderChange(int id, int value) {
         switch(id) {
            case SLIDER:
              speed = value;
              break;
         } 
    }
    
    String getLEDLabel() {
        return led ? F("Turn LED off") : F("Turn LED on"); 
    }
};

// Create an instance of your AndroidController subclass
MyAndroidController android;

boolean ledBlinkState = true;

void setup() {
    pinMode(13, OUTPUT);
    Serial.begin(9600);

    // Initialize and start the android controller
    android.begin();
}

void loop() {

    // Read sensor data from A0
    int m = analogRead(A0);

    // Send sensor data on our data channel (as String).
    android.send(android.DATA_CHANNEL, String(m));

    // If the LED is enabled from the controller, blink it
    if (android.led || ledBlinkState) {
         ledBlinkState = !ledBlinkState;
         digitalWrite(13, ledBlinkState ? HIGH : LOW);
    }

    // Need to call this periodically to retrieve data from the Android device
    android.receive();

    // speed is bound to the slider and its value between 1 and 10
    delay(100 * (10 - android.speed));
}
